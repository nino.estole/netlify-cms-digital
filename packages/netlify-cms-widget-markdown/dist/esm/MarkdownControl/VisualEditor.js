"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.mergeMediaConfig = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _debounce2 = _interopRequireDefault(require("lodash/debounce"));

var _isEmpty2 = _interopRequireDefault(require("lodash/isEmpty"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactImmutableProptypes = _interopRequireDefault(require("react-immutable-proptypes"));

var _immutable = require("immutable");

var _core = require("@emotion/core");

var _slate = require("slate");

var _slateReact = require("slate-react");

var _netlifyCmsUiDefault = require("netlify-cms-ui-default");

var _styles = require("../styles");

var _serializers = require("../serializers");

var _Toolbar = _interopRequireDefault(require("../MarkdownControl/Toolbar"));

var _renderers = require("./renderers");

var _visual = _interopRequireDefault(require("./plugins/visual"));

var _schema = _interopRequireDefault(require("./schema"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

const visualEditorStyles = ({
  minimal
}) => `
  position: relative;
  overflow: hidden;
  overflow-x: auto;
  font-family: ${_netlifyCmsUiDefault.fonts.primary};
  min-height: ${minimal ? 'auto' : _netlifyCmsUiDefault.lengths.richTextEditorMinHeight};
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-top: 0;
  margin-top: -${_styles.editorStyleVars.stickyDistanceBottom};
  padding: 0;
  display: flex;
  flex-direction: column;
  z-index: ${_netlifyCmsUiDefault.zIndex.zIndex100};
`;

const InsertionPoint = (0, _styledBase.default)("div", {
  target: "evezps90",
  label: "InsertionPoint"
})(process.env.NODE_ENV === "production" ? {
  name: "b2d31m",
  styles: "flex:1 1 auto;cursor:text;"
} : {
  name: "b2d31m",
  styles: "flex:1 1 auto;cursor:text;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9NYXJrZG93bkNvbnRyb2wvVmlzdWFsRWRpdG9yLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWlDaUMiLCJmaWxlIjoiLi4vLi4vLi4vc3JjL01hcmtkb3duQ29udHJvbC9WaXN1YWxFZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBJbW11dGFibGVQcm9wVHlwZXMgZnJvbSAncmVhY3QtaW1tdXRhYmxlLXByb3B0eXBlcyc7XG5pbXBvcnQgeyBmcm9tSlMgfSBmcm9tICdpbW11dGFibGUnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnO1xuaW1wb3J0IHsgY3NzIGFzIGNvcmVDc3MsIENsYXNzTmFtZXMgfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcbmltcG9ydCB7IGdldCwgaXNFbXB0eSwgZGVib3VuY2UgfSBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgVmFsdWUsIERvY3VtZW50LCBCbG9jaywgVGV4dCB9IGZyb20gJ3NsYXRlJztcbmltcG9ydCB7IEVkaXRvciBhcyBTbGF0ZSB9IGZyb20gJ3NsYXRlLXJlYWN0JztcbmltcG9ydCB7IGxlbmd0aHMsIGZvbnRzLCB6SW5kZXggfSBmcm9tICduZXRsaWZ5LWNtcy11aS1kZWZhdWx0JztcbmltcG9ydCB7IGVkaXRvclN0eWxlVmFycywgRWRpdG9yQ29udHJvbEJhciB9IGZyb20gJy4uL3N0eWxlcyc7XG5pbXBvcnQgeyBzbGF0ZVRvTWFya2Rvd24sIG1hcmtkb3duVG9TbGF0ZSB9IGZyb20gJy4uL3NlcmlhbGl6ZXJzJztcbmltcG9ydCBUb29sYmFyIGZyb20gJy4uL01hcmtkb3duQ29udHJvbC9Ub29sYmFyJztcbmltcG9ydCB7IHJlbmRlckJsb2NrLCByZW5kZXJJbmxpbmUsIHJlbmRlck1hcmsgfSBmcm9tICcuL3JlbmRlcmVycyc7XG5pbXBvcnQgcGx1Z2lucyBmcm9tICcuL3BsdWdpbnMvdmlzdWFsJztcbmltcG9ydCBzY2hlbWEgZnJvbSAnLi9zY2hlbWEnO1xuXG5jb25zdCB2aXN1YWxFZGl0b3JTdHlsZXMgPSAoeyBtaW5pbWFsIH0pID0+IGBcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBmb250LWZhbWlseTogJHtmb250cy5wcmltYXJ5fTtcbiAgbWluLWhlaWdodDogJHttaW5pbWFsID8gJ2F1dG8nIDogbGVuZ3Rocy5yaWNoVGV4dEVkaXRvck1pbkhlaWdodH07XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICBib3JkZXItdG9wOiAwO1xuICBtYXJnaW4tdG9wOiAtJHtlZGl0b3JTdHlsZVZhcnMuc3RpY2t5RGlzdGFuY2VCb3R0b219O1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB6LWluZGV4OiAke3pJbmRleC56SW5kZXgxMDB9O1xuYDtcblxuY29uc3QgSW5zZXJ0aW9uUG9pbnQgPSBzdHlsZWQuZGl2YFxuICBmbGV4OiAxIDEgYXV0bztcbiAgY3Vyc29yOiB0ZXh0O1xuYDtcblxuY29uc3QgY3JlYXRlRW1wdHlSYXdEb2MgPSAoKSA9PiB7XG4gIGNvbnN0IGVtcHR5VGV4dCA9IFRleHQuY3JlYXRlKCcnKTtcbiAgY29uc3QgZW1wdHlCbG9jayA9IEJsb2NrLmNyZWF0ZSh7IG9iamVjdDogJ2Jsb2NrJywgdHlwZTogJ3BhcmFncmFwaCcsIG5vZGVzOiBbZW1wdHlUZXh0XSB9KTtcbiAgcmV0dXJuIHsgbm9kZXM6IFtlbXB0eUJsb2NrXSB9O1xufTtcblxuY29uc3QgY3JlYXRlU2xhdGVWYWx1ZSA9IChyYXdWYWx1ZSwgeyB2b2lkQ29kZUJsb2NrIH0pID0+IHtcbiAgY29uc3QgcmF3RG9jID0gcmF3VmFsdWUgJiYgbWFya2Rvd25Ub1NsYXRlKHJhd1ZhbHVlLCB7IHZvaWRDb2RlQmxvY2sgfSk7XG4gIGNvbnN0IHJhd0RvY0hhc05vZGVzID0gIWlzRW1wdHkoZ2V0KHJhd0RvYywgJ25vZGVzJykpO1xuICBjb25zdCBkb2N1bWVudCA9IERvY3VtZW50LmZyb21KU09OKHJhd0RvY0hhc05vZGVzID8gcmF3RG9jIDogY3JlYXRlRW1wdHlSYXdEb2MoKSk7XG4gIHJldHVybiBWYWx1ZS5jcmVhdGUoeyBkb2N1bWVudCB9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBtZXJnZU1lZGlhQ29uZmlnID0gKGVkaXRvckNvbXBvbmVudHMsIGZpZWxkKSA9PiB7XG4gIC8vIG1lcmdlIGVkaXRvciBtZWRpYSBsaWJyYXJ5IGNvbmZpZyB0byBpbWFnZSBjb21wb25lbnRzXG4gIGlmIChlZGl0b3JDb21wb25lbnRzLmhhcygnaW1hZ2UnKSkge1xuICAgIGNvbnN0IGltYWdlQ29tcG9uZW50ID0gZWRpdG9yQ29tcG9uZW50cy5nZXQoJ2ltYWdlJyk7XG4gICAgY29uc3QgZmllbGRzID0gaW1hZ2VDb21wb25lbnQ/LmZpZWxkcztcblxuICAgIGlmIChmaWVsZHMpIHtcbiAgICAgIGltYWdlQ29tcG9uZW50LmZpZWxkcyA9IGZpZWxkcy51cGRhdGUoXG4gICAgICAgIGZpZWxkcy5maW5kSW5kZXgoZiA9PiBmLmdldCgnd2lkZ2V0JykgPT09ICdpbWFnZScpLFxuICAgICAgICBmID0+IHtcbiAgICAgICAgICAvLyBtZXJnZSBgbWVkaWFfbGlicmFyeWAgY29uZmlnXG4gICAgICAgICAgaWYgKGZpZWxkLmhhcygnbWVkaWFfbGlicmFyeScpKSB7XG4gICAgICAgICAgICBmID0gZi5zZXQoXG4gICAgICAgICAgICAgICdtZWRpYV9saWJyYXJ5JyxcbiAgICAgICAgICAgICAgZmllbGQuZ2V0KCdtZWRpYV9saWJyYXJ5JykubWVyZ2VEZWVwKGYuZ2V0KCdtZWRpYV9saWJyYXJ5JykpLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gbWVyZ2UgJ21lZGlhX2ZvbGRlcidcbiAgICAgICAgICBpZiAoZmllbGQuaGFzKCdtZWRpYV9mb2xkZXInKSAmJiAhZi5oYXMoJ21lZGlhX2ZvbGRlcicpKSB7XG4gICAgICAgICAgICBmID0gZi5zZXQoJ21lZGlhX2ZvbGRlcicsIGZpZWxkLmdldCgnbWVkaWFfZm9sZGVyJykpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBtZXJnZSAncHVibGljX2ZvbGRlcidcbiAgICAgICAgICBpZiAoZmllbGQuaGFzKCdwdWJsaWNfZm9sZGVyJykgJiYgIWYuaGFzKCdwdWJsaWNfZm9sZGVyJykpIHtcbiAgICAgICAgICAgIGYgPSBmLnNldCgncHVibGljX2ZvbGRlcicsIGZpZWxkLmdldCgncHVibGljX2ZvbGRlcicpKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGY7XG4gICAgICAgIH0sXG4gICAgICApO1xuICAgIH1cbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRWRpdG9yIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgY29uc3QgZWRpdG9yQ29tcG9uZW50cyA9IHByb3BzLmdldEVkaXRvckNvbXBvbmVudHMoKTtcbiAgICB0aGlzLnNob3J0Y29kZUNvbXBvbmVudHMgPSBlZGl0b3JDb21wb25lbnRzLmZpbHRlcigoeyB0eXBlIH0pID0+IHR5cGUgPT09ICdzaG9ydGNvZGUnKTtcbiAgICB0aGlzLmNvZGVCbG9ja0NvbXBvbmVudCA9IGZyb21KUyhlZGl0b3JDb21wb25lbnRzLmZpbmQoKHsgdHlwZSB9KSA9PiB0eXBlID09PSAnY29kZS1ibG9jaycpKTtcbiAgICB0aGlzLmVkaXRvckNvbXBvbmVudHMgPVxuICAgICAgdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfHwgZWRpdG9yQ29tcG9uZW50cy5oYXMoJ2NvZGUtYmxvY2snKVxuICAgICAgICA/IGVkaXRvckNvbXBvbmVudHNcbiAgICAgICAgOiBlZGl0b3JDb21wb25lbnRzLnNldCgnY29kZS1ibG9jaycsIHsgbGFiZWw6ICdDb2RlIEJsb2NrJywgdHlwZTogJ2NvZGUtYmxvY2snIH0pO1xuXG4gICAgbWVyZ2VNZWRpYUNvbmZpZyh0aGlzLmVkaXRvckNvbXBvbmVudHMsIHRoaXMucHJvcHMuZmllbGQpO1xuICAgIHRoaXMucmVuZGVyQmxvY2sgPSByZW5kZXJCbG9jayh7XG4gICAgICBjbGFzc05hbWVXcmFwcGVyOiBwcm9wcy5jbGFzc05hbWUsXG4gICAgICByZXNvbHZlV2lkZ2V0OiBwcm9wcy5yZXNvbHZlV2lkZ2V0LFxuICAgICAgY29kZUJsb2NrQ29tcG9uZW50OiB0aGlzLmNvZGVCbG9ja0NvbXBvbmVudCxcbiAgICB9KTtcbiAgICB0aGlzLnJlbmRlcklubGluZSA9IHJlbmRlcklubGluZSgpO1xuICAgIHRoaXMucmVuZGVyTWFyayA9IHJlbmRlck1hcmsoKTtcbiAgICB0aGlzLnNjaGVtYSA9IHNjaGVtYSh7IHZvaWRDb2RlQmxvY2s6ICEhdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfSk7XG4gICAgdGhpcy5wbHVnaW5zID0gcGx1Z2lucyh7IGdldEFzc2V0OiBwcm9wcy5nZXRBc3NldCwgcmVzb2x2ZVdpZGdldDogcHJvcHMucmVzb2x2ZVdpZGdldCB9KTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdmFsdWU6IGNyZWF0ZVNsYXRlVmFsdWUodGhpcy5wcm9wcy52YWx1ZSwgeyB2b2lkQ29kZUJsb2NrOiAhIXRoaXMuY29kZUJsb2NrQ29tcG9uZW50IH0pLFxuICAgIH07XG4gIH1cblxuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIG9uQWRkQXNzZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgZ2V0QXNzZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgb25Nb2RlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGZpZWxkOiBJbW11dGFibGVQcm9wVHlwZXMubWFwLmlzUmVxdWlyZWQsXG4gICAgZ2V0RWRpdG9yQ29tcG9uZW50czogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICB0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAgIHJldHVybiAhdGhpcy5zdGF0ZS52YWx1ZS5lcXVhbHMobmV4dFN0YXRlLnZhbHVlKTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGlmICh0aGlzLnByb3BzLnBlbmRpbmdGb2N1cykge1xuICAgICAgdGhpcy5lZGl0b3IuZm9jdXMoKTtcbiAgICAgIHRoaXMucHJvcHMucGVuZGluZ0ZvY3VzKCk7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlTWFya0NsaWNrID0gdHlwZSA9PiB7XG4gICAgdGhpcy5lZGl0b3IudG9nZ2xlTWFyayh0eXBlKS5mb2N1cygpO1xuICB9O1xuXG4gIGhhbmRsZUJsb2NrQ2xpY2sgPSB0eXBlID0+IHtcbiAgICB0aGlzLmVkaXRvci50b2dnbGVCbG9jayh0eXBlKS5mb2N1cygpO1xuICB9O1xuXG4gIGhhbmRsZUxpbmtDbGljayA9ICgpID0+IHtcbiAgICB0aGlzLmVkaXRvci50b2dnbGVMaW5rKCgpID0+IHdpbmRvdy5wcm9tcHQoJ0VudGVyIHRoZSBVUkwgb2YgdGhlIGxpbmsnKSk7XG4gIH07XG5cbiAgaGFzTWFyayA9IHR5cGUgPT4gdGhpcy5lZGl0b3IgJiYgdGhpcy5lZGl0b3IuaGFzTWFyayh0eXBlKTtcbiAgaGFzSW5saW5lID0gdHlwZSA9PiB0aGlzLmVkaXRvciAmJiB0aGlzLmVkaXRvci5oYXNJbmxpbmUodHlwZSk7XG4gIGhhc0Jsb2NrID0gdHlwZSA9PiB0aGlzLmVkaXRvciAmJiB0aGlzLmVkaXRvci5oYXNCbG9jayh0eXBlKTtcblxuICBoYW5kbGVUb2dnbGVNb2RlID0gKCkgPT4ge1xuICAgIHRoaXMucHJvcHMub25Nb2RlKCdyYXcnKTtcbiAgfTtcblxuICBoYW5kbGVJbnNlcnRTaG9ydGNvZGUgPSBwbHVnaW5Db25maWcgPT4ge1xuICAgIHRoaXMuZWRpdG9yLmluc2VydFNob3J0Y29kZShwbHVnaW5Db25maWcpO1xuICB9O1xuXG4gIGhhbmRsZUNsaWNrQmVsb3dEb2N1bWVudCA9ICgpID0+IHtcbiAgICB0aGlzLmVkaXRvci5tb3ZlVG9FbmRPZkRvY3VtZW50KCk7XG4gIH07XG5cbiAgaGFuZGxlRG9jdW1lbnRDaGFuZ2UgPSBkZWJvdW5jZShlZGl0b3IgPT4ge1xuICAgIGNvbnN0IHsgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgcmF3ID0gZWRpdG9yLnZhbHVlLmRvY3VtZW50LnRvSlMoKTtcbiAgICBjb25zdCBtYXJrZG93biA9IHNsYXRlVG9NYXJrZG93bihyYXcsIHsgdm9pZENvZGVCbG9jazogdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfSk7XG4gICAgb25DaGFuZ2UobWFya2Rvd24pO1xuICB9LCAxNTApO1xuXG4gIGhhbmRsZUNoYW5nZSA9IGVkaXRvciA9PiB7XG4gICAgaWYgKCF0aGlzLnN0YXRlLnZhbHVlLmRvY3VtZW50LmVxdWFscyhlZGl0b3IudmFsdWUuZG9jdW1lbnQpKSB7XG4gICAgICB0aGlzLmhhbmRsZURvY3VtZW50Q2hhbmdlKGVkaXRvcik7XG4gICAgfVxuICAgIHRoaXMuc2V0U3RhdGUoeyB2YWx1ZTogZWRpdG9yLnZhbHVlIH0pO1xuICB9O1xuXG4gIHByb2Nlc3NSZWYgPSByZWYgPT4ge1xuICAgIHRoaXMuZWRpdG9yID0gcmVmO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IG9uQWRkQXNzZXQsIGdldEFzc2V0LCBjbGFzc05hbWUsIGZpZWxkLCB0IH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNzcz17Y29yZUNzc2BcbiAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGB9XG4gICAgICA+XG4gICAgICAgIDxFZGl0b3JDb250cm9sQmFyPlxuICAgICAgICAgIDxUb29sYmFyXG4gICAgICAgICAgICBvbk1hcmtDbGljaz17dGhpcy5oYW5kbGVNYXJrQ2xpY2t9XG4gICAgICAgICAgICBvbkJsb2NrQ2xpY2s9e3RoaXMuaGFuZGxlQmxvY2tDbGlja31cbiAgICAgICAgICAgIG9uTGlua0NsaWNrPXt0aGlzLmhhbmRsZUxpbmtDbGlja31cbiAgICAgICAgICAgIG9uVG9nZ2xlTW9kZT17dGhpcy5oYW5kbGVUb2dnbGVNb2RlfVxuICAgICAgICAgICAgcGx1Z2lucz17dGhpcy5lZGl0b3JDb21wb25lbnRzfVxuICAgICAgICAgICAgb25TdWJtaXQ9e3RoaXMuaGFuZGxlSW5zZXJ0U2hvcnRjb2RlfVxuICAgICAgICAgICAgb25BZGRBc3NldD17b25BZGRBc3NldH1cbiAgICAgICAgICAgIGdldEFzc2V0PXtnZXRBc3NldH1cbiAgICAgICAgICAgIGJ1dHRvbnM9e2ZpZWxkLmdldCgnYnV0dG9ucycpfVxuICAgICAgICAgICAgZWRpdG9yQ29tcG9uZW50cz17ZmllbGQuZ2V0KCdlZGl0b3JDb21wb25lbnRzJyl9XG4gICAgICAgICAgICBoYXNNYXJrPXt0aGlzLmhhc01hcmt9XG4gICAgICAgICAgICBoYXNJbmxpbmU9e3RoaXMuaGFzSW5saW5lfVxuICAgICAgICAgICAgaGFzQmxvY2s9e3RoaXMuaGFzQmxvY2t9XG4gICAgICAgICAgICB0PXt0fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvRWRpdG9yQ29udHJvbEJhcj5cbiAgICAgICAgPENsYXNzTmFtZXM+XG4gICAgICAgICAgeyh7IGNzcywgY3ggfSkgPT4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2N4KFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBjc3NgXG4gICAgICAgICAgICAgICAgICAke3Zpc3VhbEVkaXRvclN0eWxlcyh7IG1pbmltYWw6IGZpZWxkLmdldCgnbWluaW1hbCcpIH0pfVxuICAgICAgICAgICAgICAgIGAsXG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxTbGF0ZVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y3NzYFxuICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTZweCAyMHB4IDA7XG4gICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS52YWx1ZX1cbiAgICAgICAgICAgICAgICByZW5kZXJCbG9jaz17dGhpcy5yZW5kZXJCbG9ja31cbiAgICAgICAgICAgICAgICByZW5kZXJJbmxpbmU9e3RoaXMucmVuZGVySW5saW5lfVxuICAgICAgICAgICAgICAgIHJlbmRlck1hcms9e3RoaXMucmVuZGVyTWFya31cbiAgICAgICAgICAgICAgICBzY2hlbWE9e3RoaXMuc2NoZW1hfVxuICAgICAgICAgICAgICAgIHBsdWdpbnM9e3RoaXMucGx1Z2luc31cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9XG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLnByb2Nlc3NSZWZ9XG4gICAgICAgICAgICAgICAgc3BlbGxDaGVja1xuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8SW5zZXJ0aW9uUG9pbnQgb25DbGljaz17dGhpcy5oYW5kbGVDbGlja0JlbG93RG9jdW1lbnR9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApfVxuICAgICAgICA8L0NsYXNzTmFtZXM+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG4iXX0= */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

const createEmptyRawDoc = () => {
  const emptyText = _slate.Text.create('');

  const emptyBlock = _slate.Block.create({
    object: 'block',
    type: 'paragraph',
    nodes: [emptyText]
  });

  return {
    nodes: [emptyBlock]
  };
};

const createSlateValue = (rawValue, {
  voidCodeBlock
}) => {
  const rawDoc = rawValue && (0, _serializers.markdownToSlate)(rawValue, {
    voidCodeBlock
  });
  const rawDocHasNodes = !(0, _isEmpty2.default)((0, _get2.default)(rawDoc, 'nodes'));

  const document = _slate.Document.fromJSON(rawDocHasNodes ? rawDoc : createEmptyRawDoc());

  return _slate.Value.create({
    document
  });
};

const mergeMediaConfig = (editorComponents, field) => {
  // merge editor media library config to image components
  if (editorComponents.has('image')) {
    const imageComponent = editorComponents.get('image');
    const fields = imageComponent === null || imageComponent === void 0 ? void 0 : imageComponent.fields;

    if (fields) {
      imageComponent.fields = fields.update(fields.findIndex(f => f.get('widget') === 'image'), f => {
        // merge `media_library` config
        if (field.has('media_library')) {
          f = f.set('media_library', field.get('media_library').mergeDeep(f.get('media_library')));
        } // merge 'media_folder'


        if (field.has('media_folder') && !f.has('media_folder')) {
          f = f.set('media_folder', field.get('media_folder'));
        } // merge 'public_folder'


        if (field.has('public_folder') && !f.has('public_folder')) {
          f = f.set('public_folder', field.get('public_folder'));
        }

        return f;
      });
    }
  }
};

exports.mergeMediaConfig = mergeMediaConfig;

var _ref = process.env.NODE_ENV === "production" ? {
  name: "t5h4ts-Editor",
  styles: "position:relative;;label:Editor;"
} : {
  name: "t5h4ts-Editor",
  styles: "position:relative;;label:Editor;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9NYXJrZG93bkNvbnRyb2wvVmlzdWFsRWRpdG9yLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXNMb0IiLCJmaWxlIjoiLi4vLi4vLi4vc3JjL01hcmtkb3duQ29udHJvbC9WaXN1YWxFZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBJbW11dGFibGVQcm9wVHlwZXMgZnJvbSAncmVhY3QtaW1tdXRhYmxlLXByb3B0eXBlcyc7XG5pbXBvcnQgeyBmcm9tSlMgfSBmcm9tICdpbW11dGFibGUnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnO1xuaW1wb3J0IHsgY3NzIGFzIGNvcmVDc3MsIENsYXNzTmFtZXMgfSBmcm9tICdAZW1vdGlvbi9jb3JlJztcbmltcG9ydCB7IGdldCwgaXNFbXB0eSwgZGVib3VuY2UgfSBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgVmFsdWUsIERvY3VtZW50LCBCbG9jaywgVGV4dCB9IGZyb20gJ3NsYXRlJztcbmltcG9ydCB7IEVkaXRvciBhcyBTbGF0ZSB9IGZyb20gJ3NsYXRlLXJlYWN0JztcbmltcG9ydCB7IGxlbmd0aHMsIGZvbnRzLCB6SW5kZXggfSBmcm9tICduZXRsaWZ5LWNtcy11aS1kZWZhdWx0JztcbmltcG9ydCB7IGVkaXRvclN0eWxlVmFycywgRWRpdG9yQ29udHJvbEJhciB9IGZyb20gJy4uL3N0eWxlcyc7XG5pbXBvcnQgeyBzbGF0ZVRvTWFya2Rvd24sIG1hcmtkb3duVG9TbGF0ZSB9IGZyb20gJy4uL3NlcmlhbGl6ZXJzJztcbmltcG9ydCBUb29sYmFyIGZyb20gJy4uL01hcmtkb3duQ29udHJvbC9Ub29sYmFyJztcbmltcG9ydCB7IHJlbmRlckJsb2NrLCByZW5kZXJJbmxpbmUsIHJlbmRlck1hcmsgfSBmcm9tICcuL3JlbmRlcmVycyc7XG5pbXBvcnQgcGx1Z2lucyBmcm9tICcuL3BsdWdpbnMvdmlzdWFsJztcbmltcG9ydCBzY2hlbWEgZnJvbSAnLi9zY2hlbWEnO1xuXG5jb25zdCB2aXN1YWxFZGl0b3JTdHlsZXMgPSAoeyBtaW5pbWFsIH0pID0+IGBcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBvdmVyZmxvdy14OiBhdXRvO1xuICBmb250LWZhbWlseTogJHtmb250cy5wcmltYXJ5fTtcbiAgbWluLWhlaWdodDogJHttaW5pbWFsID8gJ2F1dG8nIDogbGVuZ3Rocy5yaWNoVGV4dEVkaXRvck1pbkhlaWdodH07XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICBib3JkZXItdG9wOiAwO1xuICBtYXJnaW4tdG9wOiAtJHtlZGl0b3JTdHlsZVZhcnMuc3RpY2t5RGlzdGFuY2VCb3R0b219O1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB6LWluZGV4OiAke3pJbmRleC56SW5kZXgxMDB9O1xuYDtcblxuY29uc3QgSW5zZXJ0aW9uUG9pbnQgPSBzdHlsZWQuZGl2YFxuICBmbGV4OiAxIDEgYXV0bztcbiAgY3Vyc29yOiB0ZXh0O1xuYDtcblxuY29uc3QgY3JlYXRlRW1wdHlSYXdEb2MgPSAoKSA9PiB7XG4gIGNvbnN0IGVtcHR5VGV4dCA9IFRleHQuY3JlYXRlKCcnKTtcbiAgY29uc3QgZW1wdHlCbG9jayA9IEJsb2NrLmNyZWF0ZSh7IG9iamVjdDogJ2Jsb2NrJywgdHlwZTogJ3BhcmFncmFwaCcsIG5vZGVzOiBbZW1wdHlUZXh0XSB9KTtcbiAgcmV0dXJuIHsgbm9kZXM6IFtlbXB0eUJsb2NrXSB9O1xufTtcblxuY29uc3QgY3JlYXRlU2xhdGVWYWx1ZSA9IChyYXdWYWx1ZSwgeyB2b2lkQ29kZUJsb2NrIH0pID0+IHtcbiAgY29uc3QgcmF3RG9jID0gcmF3VmFsdWUgJiYgbWFya2Rvd25Ub1NsYXRlKHJhd1ZhbHVlLCB7IHZvaWRDb2RlQmxvY2sgfSk7XG4gIGNvbnN0IHJhd0RvY0hhc05vZGVzID0gIWlzRW1wdHkoZ2V0KHJhd0RvYywgJ25vZGVzJykpO1xuICBjb25zdCBkb2N1bWVudCA9IERvY3VtZW50LmZyb21KU09OKHJhd0RvY0hhc05vZGVzID8gcmF3RG9jIDogY3JlYXRlRW1wdHlSYXdEb2MoKSk7XG4gIHJldHVybiBWYWx1ZS5jcmVhdGUoeyBkb2N1bWVudCB9KTtcbn07XG5cbmV4cG9ydCBjb25zdCBtZXJnZU1lZGlhQ29uZmlnID0gKGVkaXRvckNvbXBvbmVudHMsIGZpZWxkKSA9PiB7XG4gIC8vIG1lcmdlIGVkaXRvciBtZWRpYSBsaWJyYXJ5IGNvbmZpZyB0byBpbWFnZSBjb21wb25lbnRzXG4gIGlmIChlZGl0b3JDb21wb25lbnRzLmhhcygnaW1hZ2UnKSkge1xuICAgIGNvbnN0IGltYWdlQ29tcG9uZW50ID0gZWRpdG9yQ29tcG9uZW50cy5nZXQoJ2ltYWdlJyk7XG4gICAgY29uc3QgZmllbGRzID0gaW1hZ2VDb21wb25lbnQ/LmZpZWxkcztcblxuICAgIGlmIChmaWVsZHMpIHtcbiAgICAgIGltYWdlQ29tcG9uZW50LmZpZWxkcyA9IGZpZWxkcy51cGRhdGUoXG4gICAgICAgIGZpZWxkcy5maW5kSW5kZXgoZiA9PiBmLmdldCgnd2lkZ2V0JykgPT09ICdpbWFnZScpLFxuICAgICAgICBmID0+IHtcbiAgICAgICAgICAvLyBtZXJnZSBgbWVkaWFfbGlicmFyeWAgY29uZmlnXG4gICAgICAgICAgaWYgKGZpZWxkLmhhcygnbWVkaWFfbGlicmFyeScpKSB7XG4gICAgICAgICAgICBmID0gZi5zZXQoXG4gICAgICAgICAgICAgICdtZWRpYV9saWJyYXJ5JyxcbiAgICAgICAgICAgICAgZmllbGQuZ2V0KCdtZWRpYV9saWJyYXJ5JykubWVyZ2VEZWVwKGYuZ2V0KCdtZWRpYV9saWJyYXJ5JykpLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLy8gbWVyZ2UgJ21lZGlhX2ZvbGRlcidcbiAgICAgICAgICBpZiAoZmllbGQuaGFzKCdtZWRpYV9mb2xkZXInKSAmJiAhZi5oYXMoJ21lZGlhX2ZvbGRlcicpKSB7XG4gICAgICAgICAgICBmID0gZi5zZXQoJ21lZGlhX2ZvbGRlcicsIGZpZWxkLmdldCgnbWVkaWFfZm9sZGVyJykpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBtZXJnZSAncHVibGljX2ZvbGRlcidcbiAgICAgICAgICBpZiAoZmllbGQuaGFzKCdwdWJsaWNfZm9sZGVyJykgJiYgIWYuaGFzKCdwdWJsaWNfZm9sZGVyJykpIHtcbiAgICAgICAgICAgIGYgPSBmLnNldCgncHVibGljX2ZvbGRlcicsIGZpZWxkLmdldCgncHVibGljX2ZvbGRlcicpKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGY7XG4gICAgICAgIH0sXG4gICAgICApO1xuICAgIH1cbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRWRpdG9yIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgY29uc3QgZWRpdG9yQ29tcG9uZW50cyA9IHByb3BzLmdldEVkaXRvckNvbXBvbmVudHMoKTtcbiAgICB0aGlzLnNob3J0Y29kZUNvbXBvbmVudHMgPSBlZGl0b3JDb21wb25lbnRzLmZpbHRlcigoeyB0eXBlIH0pID0+IHR5cGUgPT09ICdzaG9ydGNvZGUnKTtcbiAgICB0aGlzLmNvZGVCbG9ja0NvbXBvbmVudCA9IGZyb21KUyhlZGl0b3JDb21wb25lbnRzLmZpbmQoKHsgdHlwZSB9KSA9PiB0eXBlID09PSAnY29kZS1ibG9jaycpKTtcbiAgICB0aGlzLmVkaXRvckNvbXBvbmVudHMgPVxuICAgICAgdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfHwgZWRpdG9yQ29tcG9uZW50cy5oYXMoJ2NvZGUtYmxvY2snKVxuICAgICAgICA/IGVkaXRvckNvbXBvbmVudHNcbiAgICAgICAgOiBlZGl0b3JDb21wb25lbnRzLnNldCgnY29kZS1ibG9jaycsIHsgbGFiZWw6ICdDb2RlIEJsb2NrJywgdHlwZTogJ2NvZGUtYmxvY2snIH0pO1xuXG4gICAgbWVyZ2VNZWRpYUNvbmZpZyh0aGlzLmVkaXRvckNvbXBvbmVudHMsIHRoaXMucHJvcHMuZmllbGQpO1xuICAgIHRoaXMucmVuZGVyQmxvY2sgPSByZW5kZXJCbG9jayh7XG4gICAgICBjbGFzc05hbWVXcmFwcGVyOiBwcm9wcy5jbGFzc05hbWUsXG4gICAgICByZXNvbHZlV2lkZ2V0OiBwcm9wcy5yZXNvbHZlV2lkZ2V0LFxuICAgICAgY29kZUJsb2NrQ29tcG9uZW50OiB0aGlzLmNvZGVCbG9ja0NvbXBvbmVudCxcbiAgICB9KTtcbiAgICB0aGlzLnJlbmRlcklubGluZSA9IHJlbmRlcklubGluZSgpO1xuICAgIHRoaXMucmVuZGVyTWFyayA9IHJlbmRlck1hcmsoKTtcbiAgICB0aGlzLnNjaGVtYSA9IHNjaGVtYSh7IHZvaWRDb2RlQmxvY2s6ICEhdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfSk7XG4gICAgdGhpcy5wbHVnaW5zID0gcGx1Z2lucyh7IGdldEFzc2V0OiBwcm9wcy5nZXRBc3NldCwgcmVzb2x2ZVdpZGdldDogcHJvcHMucmVzb2x2ZVdpZGdldCB9KTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdmFsdWU6IGNyZWF0ZVNsYXRlVmFsdWUodGhpcy5wcm9wcy52YWx1ZSwgeyB2b2lkQ29kZUJsb2NrOiAhIXRoaXMuY29kZUJsb2NrQ29tcG9uZW50IH0pLFxuICAgIH07XG4gIH1cblxuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIG9uQWRkQXNzZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgZ2V0QXNzZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgb25Nb2RlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGZpZWxkOiBJbW11dGFibGVQcm9wVHlwZXMubWFwLmlzUmVxdWlyZWQsXG4gICAgZ2V0RWRpdG9yQ29tcG9uZW50czogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICB0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAgIHJldHVybiAhdGhpcy5zdGF0ZS52YWx1ZS5lcXVhbHMobmV4dFN0YXRlLnZhbHVlKTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGlmICh0aGlzLnByb3BzLnBlbmRpbmdGb2N1cykge1xuICAgICAgdGhpcy5lZGl0b3IuZm9jdXMoKTtcbiAgICAgIHRoaXMucHJvcHMucGVuZGluZ0ZvY3VzKCk7XG4gICAgfVxuICB9XG5cbiAgaGFuZGxlTWFya0NsaWNrID0gdHlwZSA9PiB7XG4gICAgdGhpcy5lZGl0b3IudG9nZ2xlTWFyayh0eXBlKS5mb2N1cygpO1xuICB9O1xuXG4gIGhhbmRsZUJsb2NrQ2xpY2sgPSB0eXBlID0+IHtcbiAgICB0aGlzLmVkaXRvci50b2dnbGVCbG9jayh0eXBlKS5mb2N1cygpO1xuICB9O1xuXG4gIGhhbmRsZUxpbmtDbGljayA9ICgpID0+IHtcbiAgICB0aGlzLmVkaXRvci50b2dnbGVMaW5rKCgpID0+IHdpbmRvdy5wcm9tcHQoJ0VudGVyIHRoZSBVUkwgb2YgdGhlIGxpbmsnKSk7XG4gIH07XG5cbiAgaGFzTWFyayA9IHR5cGUgPT4gdGhpcy5lZGl0b3IgJiYgdGhpcy5lZGl0b3IuaGFzTWFyayh0eXBlKTtcbiAgaGFzSW5saW5lID0gdHlwZSA9PiB0aGlzLmVkaXRvciAmJiB0aGlzLmVkaXRvci5oYXNJbmxpbmUodHlwZSk7XG4gIGhhc0Jsb2NrID0gdHlwZSA9PiB0aGlzLmVkaXRvciAmJiB0aGlzLmVkaXRvci5oYXNCbG9jayh0eXBlKTtcblxuICBoYW5kbGVUb2dnbGVNb2RlID0gKCkgPT4ge1xuICAgIHRoaXMucHJvcHMub25Nb2RlKCdyYXcnKTtcbiAgfTtcblxuICBoYW5kbGVJbnNlcnRTaG9ydGNvZGUgPSBwbHVnaW5Db25maWcgPT4ge1xuICAgIHRoaXMuZWRpdG9yLmluc2VydFNob3J0Y29kZShwbHVnaW5Db25maWcpO1xuICB9O1xuXG4gIGhhbmRsZUNsaWNrQmVsb3dEb2N1bWVudCA9ICgpID0+IHtcbiAgICB0aGlzLmVkaXRvci5tb3ZlVG9FbmRPZkRvY3VtZW50KCk7XG4gIH07XG5cbiAgaGFuZGxlRG9jdW1lbnRDaGFuZ2UgPSBkZWJvdW5jZShlZGl0b3IgPT4ge1xuICAgIGNvbnN0IHsgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgcmF3ID0gZWRpdG9yLnZhbHVlLmRvY3VtZW50LnRvSlMoKTtcbiAgICBjb25zdCBtYXJrZG93biA9IHNsYXRlVG9NYXJrZG93bihyYXcsIHsgdm9pZENvZGVCbG9jazogdGhpcy5jb2RlQmxvY2tDb21wb25lbnQgfSk7XG4gICAgb25DaGFuZ2UobWFya2Rvd24pO1xuICB9LCAxNTApO1xuXG4gIGhhbmRsZUNoYW5nZSA9IGVkaXRvciA9PiB7XG4gICAgaWYgKCF0aGlzLnN0YXRlLnZhbHVlLmRvY3VtZW50LmVxdWFscyhlZGl0b3IudmFsdWUuZG9jdW1lbnQpKSB7XG4gICAgICB0aGlzLmhhbmRsZURvY3VtZW50Q2hhbmdlKGVkaXRvcik7XG4gICAgfVxuICAgIHRoaXMuc2V0U3RhdGUoeyB2YWx1ZTogZWRpdG9yLnZhbHVlIH0pO1xuICB9O1xuXG4gIHByb2Nlc3NSZWYgPSByZWYgPT4ge1xuICAgIHRoaXMuZWRpdG9yID0gcmVmO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IG9uQWRkQXNzZXQsIGdldEFzc2V0LCBjbGFzc05hbWUsIGZpZWxkLCB0IH0gPSB0aGlzLnByb3BzO1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNzcz17Y29yZUNzc2BcbiAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGB9XG4gICAgICA+XG4gICAgICAgIDxFZGl0b3JDb250cm9sQmFyPlxuICAgICAgICAgIDxUb29sYmFyXG4gICAgICAgICAgICBvbk1hcmtDbGljaz17dGhpcy5oYW5kbGVNYXJrQ2xpY2t9XG4gICAgICAgICAgICBvbkJsb2NrQ2xpY2s9e3RoaXMuaGFuZGxlQmxvY2tDbGlja31cbiAgICAgICAgICAgIG9uTGlua0NsaWNrPXt0aGlzLmhhbmRsZUxpbmtDbGlja31cbiAgICAgICAgICAgIG9uVG9nZ2xlTW9kZT17dGhpcy5oYW5kbGVUb2dnbGVNb2RlfVxuICAgICAgICAgICAgcGx1Z2lucz17dGhpcy5lZGl0b3JDb21wb25lbnRzfVxuICAgICAgICAgICAgb25TdWJtaXQ9e3RoaXMuaGFuZGxlSW5zZXJ0U2hvcnRjb2RlfVxuICAgICAgICAgICAgb25BZGRBc3NldD17b25BZGRBc3NldH1cbiAgICAgICAgICAgIGdldEFzc2V0PXtnZXRBc3NldH1cbiAgICAgICAgICAgIGJ1dHRvbnM9e2ZpZWxkLmdldCgnYnV0dG9ucycpfVxuICAgICAgICAgICAgZWRpdG9yQ29tcG9uZW50cz17ZmllbGQuZ2V0KCdlZGl0b3JDb21wb25lbnRzJyl9XG4gICAgICAgICAgICBoYXNNYXJrPXt0aGlzLmhhc01hcmt9XG4gICAgICAgICAgICBoYXNJbmxpbmU9e3RoaXMuaGFzSW5saW5lfVxuICAgICAgICAgICAgaGFzQmxvY2s9e3RoaXMuaGFzQmxvY2t9XG4gICAgICAgICAgICB0PXt0fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvRWRpdG9yQ29udHJvbEJhcj5cbiAgICAgICAgPENsYXNzTmFtZXM+XG4gICAgICAgICAgeyh7IGNzcywgY3ggfSkgPT4gKFxuICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2N4KFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBjc3NgXG4gICAgICAgICAgICAgICAgICAke3Zpc3VhbEVkaXRvclN0eWxlcyh7IG1pbmltYWw6IGZpZWxkLmdldCgnbWluaW1hbCcpIH0pfVxuICAgICAgICAgICAgICAgIGAsXG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxTbGF0ZVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y3NzYFxuICAgICAgICAgICAgICAgICAgcGFkZGluZzogMTZweCAyMHB4IDA7XG4gICAgICAgICAgICAgICAgYH1cbiAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS52YWx1ZX1cbiAgICAgICAgICAgICAgICByZW5kZXJCbG9jaz17dGhpcy5yZW5kZXJCbG9ja31cbiAgICAgICAgICAgICAgICByZW5kZXJJbmxpbmU9e3RoaXMucmVuZGVySW5saW5lfVxuICAgICAgICAgICAgICAgIHJlbmRlck1hcms9e3RoaXMucmVuZGVyTWFya31cbiAgICAgICAgICAgICAgICBzY2hlbWE9e3RoaXMuc2NoZW1hfVxuICAgICAgICAgICAgICAgIHBsdWdpbnM9e3RoaXMucGx1Z2luc31cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9XG4gICAgICAgICAgICAgICAgcmVmPXt0aGlzLnByb2Nlc3NSZWZ9XG4gICAgICAgICAgICAgICAgc3BlbGxDaGVja1xuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8SW5zZXJ0aW9uUG9pbnQgb25DbGljaz17dGhpcy5oYW5kbGVDbGlja0JlbG93RG9jdW1lbnR9IC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICApfVxuICAgICAgICA8L0NsYXNzTmFtZXM+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG4iXX0= */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
};

class Editor extends _react.default.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleMarkClick", type => {
      this.editor.toggleMark(type).focus();
    });

    _defineProperty(this, "handleBlockClick", type => {
      this.editor.toggleBlock(type).focus();
    });

    _defineProperty(this, "handleLinkClick", () => {
      this.editor.toggleLink(() => window.prompt('Enter the URL of the link'));
    });

    _defineProperty(this, "hasMark", type => this.editor && this.editor.hasMark(type));

    _defineProperty(this, "hasInline", type => this.editor && this.editor.hasInline(type));

    _defineProperty(this, "hasBlock", type => this.editor && this.editor.hasBlock(type));

    _defineProperty(this, "handleToggleMode", () => {
      this.props.onMode('raw');
    });

    _defineProperty(this, "handleInsertShortcode", pluginConfig => {
      this.editor.insertShortcode(pluginConfig);
    });

    _defineProperty(this, "handleClickBelowDocument", () => {
      this.editor.moveToEndOfDocument();
    });

    _defineProperty(this, "handleDocumentChange", (0, _debounce2.default)(editor => {
      const {
        onChange
      } = this.props;
      const raw = editor.value.document.toJS();
      const markdown = (0, _serializers.slateToMarkdown)(raw, {
        voidCodeBlock: this.codeBlockComponent
      });
      onChange(markdown);
    }, 150));

    _defineProperty(this, "handleChange", editor => {
      if (!this.state.value.document.equals(editor.value.document)) {
        this.handleDocumentChange(editor);
      }

      this.setState({
        value: editor.value
      });
    });

    _defineProperty(this, "processRef", ref => {
      this.editor = ref;
    });

    const editorComponents = props.getEditorComponents();
    this.shortcodeComponents = editorComponents.filter(({
      type
    }) => type === 'shortcode');
    this.codeBlockComponent = (0, _immutable.fromJS)(editorComponents.find(({
      type
    }) => type === 'code-block'));
    this.editorComponents = this.codeBlockComponent || editorComponents.has('code-block') ? editorComponents : editorComponents.set('code-block', {
      label: 'Code Block',
      type: 'code-block'
    });
    mergeMediaConfig(this.editorComponents, this.props.field);
    this.renderBlock = (0, _renderers.renderBlock)({
      classNameWrapper: props.className,
      resolveWidget: props.resolveWidget,
      codeBlockComponent: this.codeBlockComponent
    });
    this.renderInline = (0, _renderers.renderInline)();
    this.renderMark = (0, _renderers.renderMark)();
    this.schema = (0, _schema.default)({
      voidCodeBlock: !!this.codeBlockComponent
    });
    this.plugins = (0, _visual.default)({
      getAsset: props.getAsset,
      resolveWidget: props.resolveWidget
    });
    this.state = {
      value: createSlateValue(this.props.value, {
        voidCodeBlock: !!this.codeBlockComponent
      })
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !this.state.value.equals(nextState.value);
  }

  componentDidMount() {
    if (this.props.pendingFocus) {
      this.editor.focus();
      this.props.pendingFocus();
    }
  }

  render() {
    const {
      onAddAsset,
      getAsset,
      className,
      field,
      t
    } = this.props;
    return (0, _core.jsx)("div", {
      css: _ref
    }, (0, _core.jsx)(_styles.EditorControlBar, null, (0, _core.jsx)(_Toolbar.default, {
      onMarkClick: this.handleMarkClick,
      onBlockClick: this.handleBlockClick,
      onLinkClick: this.handleLinkClick,
      onToggleMode: this.handleToggleMode,
      plugins: this.editorComponents,
      onSubmit: this.handleInsertShortcode,
      onAddAsset: onAddAsset,
      getAsset: getAsset,
      buttons: field.get('buttons'),
      editorComponents: field.get('editorComponents'),
      hasMark: this.hasMark,
      hasInline: this.hasInline,
      hasBlock: this.hasBlock,
      t: t
    })), (0, _core.jsx)(_core.ClassNames, null, ({
      css,
      cx
    }) => (0, _core.jsx)("div", {
      className: cx(className, css`
                  ${visualEditorStyles({
        minimal: field.get('minimal')
      })}
                `)
    }, (0, _core.jsx)(_slateReact.Editor, {
      className: css`
                  padding: 16px 20px 0;
                `,
      value: this.state.value,
      renderBlock: this.renderBlock,
      renderInline: this.renderInline,
      renderMark: this.renderMark,
      schema: this.schema,
      plugins: this.plugins,
      onChange: this.handleChange,
      ref: this.processRef,
      spellCheck: true
    }), (0, _core.jsx)(InsertionPoint, {
      onClick: this.handleClickBelowDocument
    }))));
  }

}

exports.default = Editor;

_defineProperty(Editor, "propTypes", {
  onAddAsset: _propTypes.default.func.isRequired,
  getAsset: _propTypes.default.func.isRequired,
  onChange: _propTypes.default.func.isRequired,
  onMode: _propTypes.default.func.isRequired,
  className: _propTypes.default.string.isRequired,
  value: _propTypes.default.string,
  field: _reactImmutableProptypes.default.map.isRequired,
  getEditorComponents: _propTypes.default.func.isRequired,
  t: _propTypes.default.func.isRequired
});