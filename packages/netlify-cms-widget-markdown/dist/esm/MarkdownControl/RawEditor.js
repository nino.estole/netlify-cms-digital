"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _debounce2 = _interopRequireDefault(require("lodash/debounce"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactImmutableProptypes = _interopRequireDefault(require("react-immutable-proptypes"));

var _core = require("@emotion/core");

var _slate = require("slate");

var _slateReact = require("slate-react");

var _slatePlainSerializer = _interopRequireDefault(require("slate-plain-serializer"));

var _isHotkey = _interopRequireDefault(require("is-hotkey"));

var _netlifyCmsUiDefault = require("netlify-cms-ui-default");

var _serializers = require("../serializers");

var _styles = require("../styles");

var _Toolbar = _interopRequireDefault(require("./Toolbar"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

const rawEditorStyles = ({
  minimal
}) => `
  position: relative;
  overflow: hidden;
  overflow-x: auto;
  min-height: ${minimal ? 'auto' : _netlifyCmsUiDefault.lengths.richTextEditorMinHeight};
  font-family: ${_netlifyCmsUiDefault.fonts.mono};
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-top: 0;
  margin-top: -${_styles.editorStyleVars.stickyDistanceBottom};
`;

const RawEditorContainer = (0, _styledBase.default)("div", {
  target: "er7tv020",
  label: "RawEditorContainer"
})(process.env.NODE_ENV === "production" ? {
  name: "79elbk",
  styles: "position:relative;"
} : {
  name: "79elbk",
  styles: "position:relative;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9NYXJrZG93bkNvbnRyb2wvUmF3RWRpdG9yLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTJCcUMiLCJmaWxlIjoiLi4vLi4vLi4vc3JjL01hcmtkb3duQ29udHJvbC9SYXdFZGl0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBJbW11dGFibGVQcm9wVHlwZXMgZnJvbSAncmVhY3QtaW1tdXRhYmxlLXByb3B0eXBlcyc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCc7XG5pbXBvcnQgeyBDbGFzc05hbWVzIH0gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQgeyBkZWJvdW5jZSB9IGZyb20gJ2xvZGFzaCc7XG5pbXBvcnQgeyBWYWx1ZSB9IGZyb20gJ3NsYXRlJztcbmltcG9ydCB7IEVkaXRvciBhcyBTbGF0ZSwgc2V0RXZlbnRUcmFuc2ZlciB9IGZyb20gJ3NsYXRlLXJlYWN0JztcbmltcG9ydCBQbGFpbiBmcm9tICdzbGF0ZS1wbGFpbi1zZXJpYWxpemVyJztcbmltcG9ydCBpc0hvdGtleSBmcm9tICdpcy1ob3RrZXknO1xuaW1wb3J0IHsgbGVuZ3RocywgZm9udHMgfSBmcm9tICduZXRsaWZ5LWNtcy11aS1kZWZhdWx0JztcbmltcG9ydCB7IG1hcmtkb3duVG9IdG1sIH0gZnJvbSAnLi4vc2VyaWFsaXplcnMnO1xuaW1wb3J0IHsgZWRpdG9yU3R5bGVWYXJzLCBFZGl0b3JDb250cm9sQmFyIH0gZnJvbSAnLi4vc3R5bGVzJztcbmltcG9ydCBUb29sYmFyIGZyb20gJy4vVG9vbGJhcic7XG5cbmNvbnN0IHJhd0VkaXRvclN0eWxlcyA9ICh7IG1pbmltYWwgfSkgPT4gYFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIG92ZXJmbG93LXg6IGF1dG87XG4gIG1pbi1oZWlnaHQ6ICR7bWluaW1hbCA/ICdhdXRvJyA6IGxlbmd0aHMucmljaFRleHRFZGl0b3JNaW5IZWlnaHR9O1xuICBmb250LWZhbWlseTogJHtmb250cy5tb25vfTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3A6IDA7XG4gIG1hcmdpbi10b3A6IC0ke2VkaXRvclN0eWxlVmFycy5zdGlja3lEaXN0YW5jZUJvdHRvbX07XG5gO1xuXG5jb25zdCBSYXdFZGl0b3JDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XG5gO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSYXdFZGl0b3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdmFsdWU6IFBsYWluLmRlc2VyaWFsaXplKHRoaXMucHJvcHMudmFsdWUgfHwgJycpLFxuICAgIH07XG4gIH1cblxuICBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcbiAgICByZXR1cm4gIXRoaXMuc3RhdGUudmFsdWUuZXF1YWxzKG5leHRTdGF0ZS52YWx1ZSk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBpZiAodGhpcy5wcm9wcy5wZW5kaW5nRm9jdXMpIHtcbiAgICAgIHRoaXMuZWRpdG9yLmZvY3VzKCk7XG4gICAgICB0aGlzLnByb3BzLnBlbmRpbmdGb2N1cygpO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUNvcHkgPSAoZXZlbnQsIGVkaXRvcikgPT4ge1xuICAgIGNvbnN0IHsgZ2V0QXNzZXQsIHJlc29sdmVXaWRnZXQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgbWFya2Rvd24gPSBQbGFpbi5zZXJpYWxpemUoVmFsdWUuY3JlYXRlKHsgZG9jdW1lbnQ6IGVkaXRvci52YWx1ZS5mcmFnbWVudCB9KSk7XG4gICAgY29uc3QgaHRtbCA9IG1hcmtkb3duVG9IdG1sKG1hcmtkb3duLCB7IGdldEFzc2V0LCByZXNvbHZlV2lkZ2V0IH0pO1xuICAgIHNldEV2ZW50VHJhbnNmZXIoZXZlbnQsICd0ZXh0JywgbWFya2Rvd24pO1xuICAgIHNldEV2ZW50VHJhbnNmZXIoZXZlbnQsICdodG1sJywgaHRtbCk7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgfTtcblxuICBoYW5kbGVDdXQgPSAoZXZlbnQsIGVkaXRvciwgbmV4dCkgPT4ge1xuICAgIHRoaXMuaGFuZGxlQ29weShldmVudCwgZWRpdG9yLCBuZXh0KTtcbiAgICBlZGl0b3IuZGVsZXRlKCk7XG4gIH07XG5cbiAgaGFuZGxlUGFzdGUgPSAoZXZlbnQsIGVkaXRvciwgbmV4dCkgPT4ge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgZGF0YSA9IGV2ZW50LmNsaXBib2FyZERhdGE7XG4gICAgaWYgKGlzSG90a2V5KCdzaGlmdCcsIGV2ZW50KSkge1xuICAgICAgcmV0dXJuIG5leHQoKTtcbiAgICB9XG5cbiAgICBjb25zdCB2YWx1ZSA9IFBsYWluLmRlc2VyaWFsaXplKGRhdGEuZ2V0RGF0YSgndGV4dC9wbGFpbicpKTtcbiAgICByZXR1cm4gZWRpdG9yLmluc2VydEZyYWdtZW50KHZhbHVlLmRvY3VtZW50KTtcbiAgfTtcblxuICBoYW5kbGVDaGFuZ2UgPSBlZGl0b3IgPT4ge1xuICAgIGlmICghdGhpcy5zdGF0ZS52YWx1ZS5kb2N1bWVudC5lcXVhbHMoZWRpdG9yLnZhbHVlLmRvY3VtZW50KSkge1xuICAgICAgdGhpcy5oYW5kbGVEb2N1bWVudENoYW5nZShlZGl0b3IpO1xuICAgIH1cbiAgICB0aGlzLnNldFN0YXRlKHsgdmFsdWU6IGVkaXRvci52YWx1ZSB9KTtcbiAgfTtcblxuICAvKipcbiAgICogV2hlbiB0aGUgZG9jdW1lbnQgdmFsdWUgY2hhbmdlcywgc2VyaWFsaXplIGZyb20gU2xhdGUncyBBU1QgYmFjayB0byBwbGFpblxuICAgKiB0ZXh0ICh3aGljaCBpcyBNYXJrZG93bikgYW5kIHBhc3MgdGhhdCB1cCBhcyB0aGUgbmV3IHZhbHVlLlxuICAgKi9cbiAgaGFuZGxlRG9jdW1lbnRDaGFuZ2UgPSBkZWJvdW5jZShlZGl0b3IgPT4ge1xuICAgIGNvbnN0IHZhbHVlID0gUGxhaW4uc2VyaWFsaXplKGVkaXRvci52YWx1ZSk7XG4gICAgdGhpcy5wcm9wcy5vbkNoYW5nZSh2YWx1ZSk7XG4gIH0sIDE1MCk7XG5cbiAgaGFuZGxlVG9nZ2xlTW9kZSA9ICgpID0+IHtcbiAgICB0aGlzLnByb3BzLm9uTW9kZSgndmlzdWFsJyk7XG4gIH07XG5cbiAgcHJvY2Vzc1JlZiA9IHJlZiA9PiB7XG4gICAgdGhpcy5lZGl0b3IgPSByZWY7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NOYW1lLCBmaWVsZCwgdCB9ID0gdGhpcy5wcm9wcztcbiAgICByZXR1cm4gKFxuICAgICAgPFJhd0VkaXRvckNvbnRhaW5lcj5cbiAgICAgICAgPEVkaXRvckNvbnRyb2xCYXI+XG4gICAgICAgICAgPFRvb2xiYXJcbiAgICAgICAgICAgIG9uVG9nZ2xlTW9kZT17dGhpcy5oYW5kbGVUb2dnbGVNb2RlfVxuICAgICAgICAgICAgYnV0dG9ucz17ZmllbGQuZ2V0KCdidXR0b25zJyl9XG4gICAgICAgICAgICBkaXNhYmxlZFxuICAgICAgICAgICAgcmF3TW9kZVxuICAgICAgICAgICAgdD17dH1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0VkaXRvckNvbnRyb2xCYXI+XG4gICAgICAgIDxDbGFzc05hbWVzPlxuICAgICAgICAgIHsoeyBjc3MsIGN4IH0pID0+IChcbiAgICAgICAgICAgIDxTbGF0ZVxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2N4KFxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgICBjc3NgXG4gICAgICAgICAgICAgICAgICAke3Jhd0VkaXRvclN0eWxlcyh7IG1pbmltYWw6IGZpZWxkLmdldCgnbWluaW1hbCcpIH0pfVxuICAgICAgICAgICAgICAgIGAsXG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLnZhbHVlfVxuICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9XG4gICAgICAgICAgICAgIG9uUGFzdGU9e3RoaXMuaGFuZGxlUGFzdGV9XG4gICAgICAgICAgICAgIG9uQ3V0PXt0aGlzLmhhbmRsZUN1dH1cbiAgICAgICAgICAgICAgb25Db3B5PXt0aGlzLmhhbmRsZUNvcHl9XG4gICAgICAgICAgICAgIHJlZj17dGhpcy5wcm9jZXNzUmVmfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICApfVxuICAgICAgICA8L0NsYXNzTmFtZXM+XG4gICAgICA8L1Jhd0VkaXRvckNvbnRhaW5lcj5cbiAgICApO1xuICB9XG59XG5cblJhd0VkaXRvci5wcm9wVHlwZXMgPSB7XG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICBvbk1vZGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZmllbGQ6IEltbXV0YWJsZVByb3BUeXBlcy5tYXAuaXNSZXF1aXJlZCxcbiAgdDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG4iXX0= */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

class RawEditor extends _react.default.Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "handleCopy", (event, editor) => {
      const {
        getAsset,
        resolveWidget
      } = this.props;

      const markdown = _slatePlainSerializer.default.serialize(_slate.Value.create({
        document: editor.value.fragment
      }));

      const html = (0, _serializers.markdownToHtml)(markdown, {
        getAsset,
        resolveWidget
      });
      (0, _slateReact.setEventTransfer)(event, 'text', markdown);
      (0, _slateReact.setEventTransfer)(event, 'html', html);
      event.preventDefault();
    });

    _defineProperty(this, "handleCut", (event, editor, next) => {
      this.handleCopy(event, editor, next);
      editor.delete();
    });

    _defineProperty(this, "handlePaste", (event, editor, next) => {
      event.preventDefault();
      const data = event.clipboardData;

      if ((0, _isHotkey.default)('shift', event)) {
        return next();
      }

      const value = _slatePlainSerializer.default.deserialize(data.getData('text/plain'));

      return editor.insertFragment(value.document);
    });

    _defineProperty(this, "handleChange", editor => {
      if (!this.state.value.document.equals(editor.value.document)) {
        this.handleDocumentChange(editor);
      }

      this.setState({
        value: editor.value
      });
    });

    _defineProperty(this, "handleDocumentChange", (0, _debounce2.default)(editor => {
      const value = _slatePlainSerializer.default.serialize(editor.value);

      this.props.onChange(value);
    }, 150));

    _defineProperty(this, "handleToggleMode", () => {
      this.props.onMode('visual');
    });

    _defineProperty(this, "processRef", ref => {
      this.editor = ref;
    });

    this.state = {
      value: _slatePlainSerializer.default.deserialize(this.props.value || '')
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !this.state.value.equals(nextState.value);
  }

  componentDidMount() {
    if (this.props.pendingFocus) {
      this.editor.focus();
      this.props.pendingFocus();
    }
  }

  render() {
    const {
      className,
      field,
      t
    } = this.props;
    return (0, _core.jsx)(RawEditorContainer, null, (0, _core.jsx)(_styles.EditorControlBar, null, (0, _core.jsx)(_Toolbar.default, {
      onToggleMode: this.handleToggleMode,
      buttons: field.get('buttons'),
      disabled: true,
      rawMode: true,
      t: t
    })), (0, _core.jsx)(_core.ClassNames, null, ({
      css,
      cx
    }) => (0, _core.jsx)(_slateReact.Editor, {
      className: cx(className, css`
                  ${rawEditorStyles({
        minimal: field.get('minimal')
      })}
                `),
      value: this.state.value,
      onChange: this.handleChange,
      onPaste: this.handlePaste,
      onCut: this.handleCut,
      onCopy: this.handleCopy,
      ref: this.processRef
    })));
  }

}

exports.default = RawEditor;
RawEditor.propTypes = {
  onChange: _propTypes.default.func.isRequired,
  onMode: _propTypes.default.func.isRequired,
  className: _propTypes.default.string.isRequired,
  value: _propTypes.default.string,
  field: _reactImmutableProptypes.default.map.isRequired,
  t: _propTypes.default.func.isRequired
};