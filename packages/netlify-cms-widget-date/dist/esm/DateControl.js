"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _once2 = _interopRequireDefault(require("lodash/once"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _core = require("@emotion/core");

var _reactDatetime = _interopRequireDefault(require("react-datetime/css/react-datetime.css"));

var _reactDatetime2 = _interopRequireDefault(require("react-datetime"));

var _moment = _interopRequireDefault(require("moment"));

var _commonTags = require("common-tags");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const warnDeprecated = (0, _once2.default)(() => console.warn((0, _commonTags.oneLine)`
  Netlify CMS config: the date widget has been deprecated and will
  be removed in the next major release. Please use the datetime widget instead.
`));
/**
 * `date` widget is deprecated in favor of the `datetime` widget
 */

class DateControl extends _react.default.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "formats", this.getFormats());

    _defineProperty(this, "defaultValue", this.getDefaultValue());

    _defineProperty(this, "isValidDate", datetime => _moment.default.isMoment(datetime) || datetime instanceof Date || datetime === '');

    _defineProperty(this, "handleChange", datetime => {
      /**
       * Set the date only if it is valid.
       */
      if (!this.isValidDate(datetime)) {
        return;
      }

      const {
        onChange
      } = this.props;
      const {
        format
      } = this.formats;
      /**
       * Produce a formatted string only if a format is set in the config.
       * Otherwise produce a date object.
       */

      if (format) {
        const formattedValue = datetime ? (0, _moment.default)(datetime).format(format) : '';
        onChange(formattedValue);
      } else {
        const value = _moment.default.isMoment(datetime) ? datetime.toDate() : datetime;
        onChange(value);
      }
    });

    _defineProperty(this, "onBlur", datetime => {
      const {
        setInactiveStyle
      } = this.props;

      if (!this.isValidDate(datetime)) {
        const parsedDate = (0, _moment.default)(datetime);

        if (parsedDate.isValid()) {
          this.handleChange(datetime);
        } else {
          window.alert('The date you entered is invalid.');
        }
      }

      setInactiveStyle();
    });
  }

  getFormats() {
    const {
      field,
      includeTime
    } = this.props;
    const format = field.get('format'); // dateFormat and timeFormat are strictly for modifying
    // input field with the date/time pickers

    const dateFormat = field.get('dateFormat'); // show time-picker? false hides it, true shows it using default format

    let timeFormat = field.get('timeFormat');

    if (typeof timeFormat === 'undefined') {
      timeFormat = !!includeTime;
    }

    return {
      format,
      dateFormat,
      timeFormat
    };
  }

  getDefaultValue() {
    const {
      field
    } = this.props;
    const defaultValue = field.get('default');
    return defaultValue;
  }

  componentDidMount() {
    warnDeprecated();
    const {
      value
    } = this.props;
    /**
     * Set the current date as default value if no default value is provided. An
     * empty string means the value is intentionally blank.
     */

    if (value === undefined) {
      setTimeout(() => {
        this.handleChange(this.defaultValue === undefined ? new Date() : this.defaultValue);
      }, 0);
    }
  } // Date is valid if datetime is a moment or Date object otherwise it's a string.
  // Handle the empty case, if the user wants to empty the field.


  render() {
    const {
      forID,
      value,
      classNameWrapper,
      setActiveStyle
    } = this.props;
    const {
      format,
      dateFormat,
      timeFormat
    } = this.formats;
    return (0, _core.jsx)("div", {
      css: /*#__PURE__*/(0, _core.css)(_reactDatetime.default, ";;label:DateControl;" + (process.env.NODE_ENV === "production" ? "" : "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9EYXRlQ29udHJvbC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUE4SGdCIiwiZmlsZSI6Ii4uLy4uL3NyYy9EYXRlQ29udHJvbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgY3NzIH0gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQgcmVhY3REYXRlVGltZVN0eWxlcyBmcm9tICdyZWFjdC1kYXRldGltZS9jc3MvcmVhY3QtZGF0ZXRpbWUuY3NzJztcbmltcG9ydCBEYXRlVGltZSBmcm9tICdyZWFjdC1kYXRldGltZSc7XG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XG5cbmltcG9ydCB7IG9uY2UgfSBmcm9tICdsb2Rhc2gnO1xuaW1wb3J0IHsgb25lTGluZSB9IGZyb20gJ2NvbW1vbi10YWdzJztcblxuY29uc3Qgd2FybkRlcHJlY2F0ZWQgPSBvbmNlKCgpID0+XG4gIGNvbnNvbGUud2FybihvbmVMaW5lYFxuICBOZXRsaWZ5IENNUyBjb25maWc6IHRoZSBkYXRlIHdpZGdldCBoYXMgYmVlbiBkZXByZWNhdGVkIGFuZCB3aWxsXG4gIGJlIHJlbW92ZWQgaW4gdGhlIG5leHQgbWFqb3IgcmVsZWFzZS4gUGxlYXNlIHVzZSB0aGUgZGF0ZXRpbWUgd2lkZ2V0IGluc3RlYWQuXG5gKSxcbik7XG5cbi8qKlxuICogYGRhdGVgIHdpZGdldCBpcyBkZXByZWNhdGVkIGluIGZhdm9yIG9mIHRoZSBgZGF0ZXRpbWVgIHdpZGdldFxuICovXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEYXRlQ29udHJvbCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgZmllbGQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBmb3JJRDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICBjbGFzc05hbWVXcmFwcGVyOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgc2V0QWN0aXZlU3R5bGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgc2V0SW5hY3RpdmVTdHlsZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLm9iamVjdCwgUHJvcFR5cGVzLnN0cmluZ10pLFxuICAgIGluY2x1ZGVUaW1lOiBQcm9wVHlwZXMuYm9vbCxcbiAgfTtcblxuICBnZXRGb3JtYXRzKCkge1xuICAgIGNvbnN0IHsgZmllbGQsIGluY2x1ZGVUaW1lIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IGZvcm1hdCA9IGZpZWxkLmdldCgnZm9ybWF0Jyk7XG5cbiAgICAvLyBkYXRlRm9ybWF0IGFuZCB0aW1lRm9ybWF0IGFyZSBzdHJpY3RseSBmb3IgbW9kaWZ5aW5nXG4gICAgLy8gaW5wdXQgZmllbGQgd2l0aCB0aGUgZGF0ZS90aW1lIHBpY2tlcnNcbiAgICBjb25zdCBkYXRlRm9ybWF0ID0gZmllbGQuZ2V0KCdkYXRlRm9ybWF0Jyk7XG4gICAgLy8gc2hvdyB0aW1lLXBpY2tlcj8gZmFsc2UgaGlkZXMgaXQsIHRydWUgc2hvd3MgaXQgdXNpbmcgZGVmYXVsdCBmb3JtYXRcbiAgICBsZXQgdGltZUZvcm1hdCA9IGZpZWxkLmdldCgndGltZUZvcm1hdCcpO1xuICAgIGlmICh0eXBlb2YgdGltZUZvcm1hdCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRpbWVGb3JtYXQgPSAhIWluY2x1ZGVUaW1lO1xuICAgIH1cblxuICAgIHJldHVybiB7XG4gICAgICBmb3JtYXQsXG4gICAgICBkYXRlRm9ybWF0LFxuICAgICAgdGltZUZvcm1hdCxcbiAgICB9O1xuICB9XG5cbiAgZ2V0RGVmYXVsdFZhbHVlKCkge1xuICAgIGNvbnN0IHsgZmllbGQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgZGVmYXVsdFZhbHVlID0gZmllbGQuZ2V0KCdkZWZhdWx0Jyk7XG4gICAgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcbiAgfVxuXG4gIGZvcm1hdHMgPSB0aGlzLmdldEZvcm1hdHMoKTtcbiAgZGVmYXVsdFZhbHVlID0gdGhpcy5nZXREZWZhdWx0VmFsdWUoKTtcblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB3YXJuRGVwcmVjYXRlZCgpO1xuICAgIGNvbnN0IHsgdmFsdWUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICAvKipcbiAgICAgKiBTZXQgdGhlIGN1cnJlbnQgZGF0ZSBhcyBkZWZhdWx0IHZhbHVlIGlmIG5vIGRlZmF1bHQgdmFsdWUgaXMgcHJvdmlkZWQuIEFuXG4gICAgICogZW1wdHkgc3RyaW5nIG1lYW5zIHRoZSB2YWx1ZSBpcyBpbnRlbnRpb25hbGx5IGJsYW5rLlxuICAgICAqL1xuICAgIGlmICh2YWx1ZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5oYW5kbGVDaGFuZ2UodGhpcy5kZWZhdWx0VmFsdWUgPT09IHVuZGVmaW5lZCA/IG5ldyBEYXRlKCkgOiB0aGlzLmRlZmF1bHRWYWx1ZSk7XG4gICAgICB9LCAwKTtcbiAgICB9XG4gIH1cblxuICAvLyBEYXRlIGlzIHZhbGlkIGlmIGRhdGV0aW1lIGlzIGEgbW9tZW50IG9yIERhdGUgb2JqZWN0IG90aGVyd2lzZSBpdCdzIGEgc3RyaW5nLlxuICAvLyBIYW5kbGUgdGhlIGVtcHR5IGNhc2UsIGlmIHRoZSB1c2VyIHdhbnRzIHRvIGVtcHR5IHRoZSBmaWVsZC5cbiAgaXNWYWxpZERhdGUgPSBkYXRldGltZSA9PlxuICAgIG1vbWVudC5pc01vbWVudChkYXRldGltZSkgfHwgZGF0ZXRpbWUgaW5zdGFuY2VvZiBEYXRlIHx8IGRhdGV0aW1lID09PSAnJztcblxuICBoYW5kbGVDaGFuZ2UgPSBkYXRldGltZSA9PiB7XG4gICAgLyoqXG4gICAgICogU2V0IHRoZSBkYXRlIG9ubHkgaWYgaXQgaXMgdmFsaWQuXG4gICAgICovXG4gICAgaWYgKCF0aGlzLmlzVmFsaWREYXRlKGRhdGV0aW1lKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IHsgb25DaGFuZ2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBmb3JtYXQgfSA9IHRoaXMuZm9ybWF0cztcblxuICAgIC8qKlxuICAgICAqIFByb2R1Y2UgYSBmb3JtYXR0ZWQgc3RyaW5nIG9ubHkgaWYgYSBmb3JtYXQgaXMgc2V0IGluIHRoZSBjb25maWcuXG4gICAgICogT3RoZXJ3aXNlIHByb2R1Y2UgYSBkYXRlIG9iamVjdC5cbiAgICAgKi9cbiAgICBpZiAoZm9ybWF0KSB7XG4gICAgICBjb25zdCBmb3JtYXR0ZWRWYWx1ZSA9IGRhdGV0aW1lID8gbW9tZW50KGRhdGV0aW1lKS5mb3JtYXQoZm9ybWF0KSA6ICcnO1xuICAgICAgb25DaGFuZ2UoZm9ybWF0dGVkVmFsdWUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zdCB2YWx1ZSA9IG1vbWVudC5pc01vbWVudChkYXRldGltZSkgPyBkYXRldGltZS50b0RhdGUoKSA6IGRhdGV0aW1lO1xuICAgICAgb25DaGFuZ2UodmFsdWUpO1xuICAgIH1cbiAgfTtcblxuICBvbkJsdXIgPSBkYXRldGltZSA9PiB7XG4gICAgY29uc3QgeyBzZXRJbmFjdGl2ZVN0eWxlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgaWYgKCF0aGlzLmlzVmFsaWREYXRlKGRhdGV0aW1lKSkge1xuICAgICAgY29uc3QgcGFyc2VkRGF0ZSA9IG1vbWVudChkYXRldGltZSk7XG5cbiAgICAgIGlmIChwYXJzZWREYXRlLmlzVmFsaWQoKSkge1xuICAgICAgICB0aGlzLmhhbmRsZUNoYW5nZShkYXRldGltZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB3aW5kb3cuYWxlcnQoJ1RoZSBkYXRlIHlvdSBlbnRlcmVkIGlzIGludmFsaWQuJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgc2V0SW5hY3RpdmVTdHlsZSgpO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGZvcklELCB2YWx1ZSwgY2xhc3NOYW1lV3JhcHBlciwgc2V0QWN0aXZlU3R5bGUgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBmb3JtYXQsIGRhdGVGb3JtYXQsIHRpbWVGb3JtYXQgfSA9IHRoaXMuZm9ybWF0cztcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAke3JlYWN0RGF0ZVRpbWVTdHlsZXN9O1xuICAgICAgICBgfVxuICAgICAgPlxuICAgICAgICA8RGF0ZVRpbWVcbiAgICAgICAgICBkYXRlRm9ybWF0PXtkYXRlRm9ybWF0fVxuICAgICAgICAgIHRpbWVGb3JtYXQ9e3RpbWVGb3JtYXR9XG4gICAgICAgICAgdmFsdWU9e21vbWVudCh2YWx1ZSwgZm9ybWF0KX1cbiAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9XG4gICAgICAgICAgb25Gb2N1cz17c2V0QWN0aXZlU3R5bGV9XG4gICAgICAgICAgb25CbHVyPXt0aGlzLm9uQmx1cn1cbiAgICAgICAgICBpbnB1dFByb3BzPXt7IGNsYXNzTmFtZTogY2xhc3NOYW1lV3JhcHBlciwgaWQ6IGZvcklEIH19XG4gICAgICAgIC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG4iXX0= */"))
    }, (0, _core.jsx)(_reactDatetime2.default, {
      dateFormat: dateFormat,
      timeFormat: timeFormat,
      value: (0, _moment.default)(value, format),
      onChange: this.handleChange,
      onFocus: setActiveStyle,
      onBlur: this.onBlur,
      inputProps: {
        className: classNameWrapper,
        id: forID
      }
    }));
  }

}

exports.default = DateControl;

_defineProperty(DateControl, "propTypes", {
  field: _propTypes.default.object.isRequired,
  forID: _propTypes.default.string,
  onChange: _propTypes.default.func.isRequired,
  classNameWrapper: _propTypes.default.string.isRequired,
  setActiveStyle: _propTypes.default.func.isRequired,
  setInactiveStyle: _propTypes.default.func.isRequired,
  value: _propTypes.default.oneOfType([_propTypes.default.object, _propTypes.default.string]),
  includeTime: _propTypes.default.bool
});