"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactImmutableProptypes = _interopRequireDefault(require("react-immutable-proptypes"));

var _EditorControl = _interopRequireDefault(require("./EditorControl"));

var _core = require("@emotion/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

const ControlPaneContainer = (0, _styledBase.default)("div", {
  target: "e1tnasa20",
  label: "ControlPaneContainer"
})(process.env.NODE_ENV === "production" ? {
  name: "78woh1",
  styles: "max-width:800px;margin:0 auto;padding-bottom:16px;font-size:16px;"
} : {
  name: "78woh1",
  styles: "max-width:800px;margin:0 auto;padding-bottom:16px;font-size:16px;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0VkaXRvci9FZGl0b3JDb250cm9sUGFuZS9FZGl0b3JDb250cm9sUGFuZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNdUMiLCJmaWxlIjoiLi4vLi4vLi4vLi4vLi4vc3JjL2NvbXBvbmVudHMvRWRpdG9yL0VkaXRvckNvbnRyb2xQYW5lL0VkaXRvckNvbnRyb2xQYW5lLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgSW1tdXRhYmxlUHJvcFR5cGVzIGZyb20gJ3JlYWN0LWltbXV0YWJsZS1wcm9wdHlwZXMnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnO1xuaW1wb3J0IEVkaXRvckNvbnRyb2wgZnJvbSAnLi9FZGl0b3JDb250cm9sJztcblxuY29uc3QgQ29udHJvbFBhbmVDb250YWluZXIgPSBzdHlsZWQuZGl2YFxuICBtYXgtd2lkdGg6IDgwMHB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZy1ib3R0b206IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTZweDtcbmA7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENvbnRyb2xQYW5lIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50IHtcbiAgY29tcG9uZW50VmFsaWRhdGUgPSB7fTtcblxuICBjb250cm9sUmVmKGZpZWxkLCB3cmFwcGVkQ29udHJvbCkge1xuICAgIGlmICghd3JhcHBlZENvbnRyb2wpIHJldHVybjtcbiAgICBjb25zdCBuYW1lID0gZmllbGQuZ2V0KCduYW1lJyk7XG5cbiAgICB0aGlzLmNvbXBvbmVudFZhbGlkYXRlW25hbWVdID1cbiAgICAgIHdyYXBwZWRDb250cm9sLmlubmVyV3JhcHBlZENvbnRyb2w/LnZhbGlkYXRlIHx8IHdyYXBwZWRDb250cm9sLnZhbGlkYXRlO1xuICB9XG5cbiAgdmFsaWRhdGUgPSAoKSA9PiB7XG4gICAgdGhpcy5wcm9wcy5maWVsZHMuZm9yRWFjaChmaWVsZCA9PiB7XG4gICAgICBpZiAoZmllbGQuZ2V0KCd3aWRnZXQnKSA9PT0gJ2hpZGRlbicpIHJldHVybjtcbiAgICAgIHRoaXMuY29tcG9uZW50VmFsaWRhdGVbZmllbGQuZ2V0KCduYW1lJyldKCk7XG4gICAgfSk7XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGNvbGxlY3Rpb24sXG4gICAgICBmaWVsZHMsXG4gICAgICBlbnRyeSxcbiAgICAgIGZpZWxkc01ldGFEYXRhLFxuICAgICAgZmllbGRzRXJyb3JzLFxuICAgICAgb25DaGFuZ2UsXG4gICAgICBvblZhbGlkYXRlLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgaWYgKCFjb2xsZWN0aW9uIHx8ICFmaWVsZHMpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGlmIChlbnRyeS5zaXplID09PSAwIHx8IGVudHJ5LmdldCgncGFydGlhbCcpID09PSB0cnVlKSB7XG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICByZXR1cm4gKFxuICAgICAgPENvbnRyb2xQYW5lQ29udGFpbmVyPlxuICAgICAgICB7ZmllbGRzLm1hcCgoZmllbGQsIGkpID0+IHtcbiAgICAgICAgICByZXR1cm4gZmllbGQuZ2V0KCd3aWRnZXQnKSA9PT0gJ2hpZGRlbicgPyBudWxsIDogKFxuICAgICAgICAgICAgPEVkaXRvckNvbnRyb2xcbiAgICAgICAgICAgICAga2V5PXtpfVxuICAgICAgICAgICAgICBmaWVsZD17ZmllbGR9XG4gICAgICAgICAgICAgIHZhbHVlPXtcbiAgICAgICAgICAgICAgICBmaWVsZC5nZXQoJ21ldGEnKVxuICAgICAgICAgICAgICAgICAgPyBlbnRyeS5nZXRJbihbJ21ldGEnLCBmaWVsZC5nZXQoJ25hbWUnKV0pXG4gICAgICAgICAgICAgICAgICA6IGVudHJ5LmdldEluKFsnZGF0YScsIGZpZWxkLmdldCgnbmFtZScpXSlcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBmaWVsZHNNZXRhRGF0YT17ZmllbGRzTWV0YURhdGF9XG4gICAgICAgICAgICAgIGZpZWxkc0Vycm9ycz17ZmllbGRzRXJyb3JzfVxuICAgICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgICAgICAgIG9uVmFsaWRhdGU9e29uVmFsaWRhdGV9XG4gICAgICAgICAgICAgIHByb2Nlc3NDb250cm9sUmVmPXt0aGlzLmNvbnRyb2xSZWYuYmluZCh0aGlzKX1cbiAgICAgICAgICAgICAgY29udHJvbFJlZj17dGhpcy5jb250cm9sUmVmfVxuICAgICAgICAgICAgICBlbnRyeT17ZW50cnl9XG4gICAgICAgICAgICAgIGNvbGxlY3Rpb249e2NvbGxlY3Rpb259XG4gICAgICAgICAgICAvPlxuICAgICAgICAgICk7XG4gICAgICAgIH0pfVxuICAgICAgPC9Db250cm9sUGFuZUNvbnRhaW5lcj5cbiAgICApO1xuICB9XG59XG5cbkNvbnRyb2xQYW5lLnByb3BUeXBlcyA9IHtcbiAgY29sbGVjdGlvbjogSW1tdXRhYmxlUHJvcFR5cGVzLm1hcC5pc1JlcXVpcmVkLFxuICBlbnRyeTogSW1tdXRhYmxlUHJvcFR5cGVzLm1hcC5pc1JlcXVpcmVkLFxuICBmaWVsZHM6IEltbXV0YWJsZVByb3BUeXBlcy5saXN0LmlzUmVxdWlyZWQsXG4gIGZpZWxkc01ldGFEYXRhOiBJbW11dGFibGVQcm9wVHlwZXMubWFwLmlzUmVxdWlyZWQsXG4gIGZpZWxkc0Vycm9yczogSW1tdXRhYmxlUHJvcFR5cGVzLm1hcC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgb25WYWxpZGF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG4iXX0= */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

class ControlPane extends _react.default.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "componentValidate", {});

    _defineProperty(this, "validate", () => {
      this.props.fields.forEach(field => {
        if (field.get('widget') === 'hidden') return;
        this.componentValidate[field.get('name')]();
      });
    });
  }

  controlRef(field, wrappedControl) {
    var _wrappedControl$inner;

    if (!wrappedControl) return;
    const name = field.get('name');
    this.componentValidate[name] = ((_wrappedControl$inner = wrappedControl.innerWrappedControl) === null || _wrappedControl$inner === void 0 ? void 0 : _wrappedControl$inner.validate) || wrappedControl.validate;
  }

  render() {
    const {
      collection,
      fields,
      entry,
      fieldsMetaData,
      fieldsErrors,
      onChange,
      onValidate
    } = this.props;

    if (!collection || !fields) {
      return null;
    }

    if (entry.size === 0 || entry.get('partial') === true) {
      return null;
    }

    return (0, _core.jsx)(ControlPaneContainer, null, fields.map((field, i) => {
      return field.get('widget') === 'hidden' ? null : (0, _core.jsx)(_EditorControl.default, {
        key: i,
        field: field,
        value: field.get('meta') ? entry.getIn(['meta', field.get('name')]) : entry.getIn(['data', field.get('name')]),
        fieldsMetaData: fieldsMetaData,
        fieldsErrors: fieldsErrors,
        onChange: onChange,
        onValidate: onValidate,
        processControlRef: this.controlRef.bind(this),
        controlRef: this.controlRef,
        entry: entry,
        collection: collection
      });
    }));
  }

}

exports.default = ControlPane;
ControlPane.propTypes = {
  collection: _reactImmutableProptypes.default.map.isRequired,
  entry: _reactImmutableProptypes.default.map.isRequired,
  fields: _reactImmutableProptypes.default.list.isRequired,
  fieldsMetaData: _reactImmutableProptypes.default.map.isRequired,
  fieldsErrors: _reactImmutableProptypes.default.map.isRequired,
  onChange: _propTypes.default.func.isRequired,
  onValidate: _propTypes.default.func.isRequired
};