"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

var _ViewStyleControl = _interopRequireDefault(require("./ViewStyleControl"));

var _SortControl = _interopRequireDefault(require("./SortControl"));

var _FilterControl = _interopRequireDefault(require("./FilterControl"));

var _netlifyCmsUiDefault = require("netlify-cms-ui-default");

var _core = require("@emotion/core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const CollectionControlsContainer = (0, _styledBase.default)("div", {
  target: "e17kb5zm0",
  label: "CollectionControlsContainer"
})("display:flex;align-items:center;flex-direction:row-reverse;margin-top:22px;width:", _netlifyCmsUiDefault.lengths.topCardWidth, ";max-width:100%;& > div{margin-left:6px;}" + (process.env.NODE_ENV === "production" ? "" : "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0NvbGxlY3Rpb24vQ29sbGVjdGlvbkNvbnRyb2xzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQU84QyIsImZpbGUiOiIuLi8uLi8uLi8uLi9zcmMvY29tcG9uZW50cy9Db2xsZWN0aW9uL0NvbGxlY3Rpb25Db250cm9scy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCc7XG5pbXBvcnQgVmlld1N0eWxlQ29udHJvbCBmcm9tICcuL1ZpZXdTdHlsZUNvbnRyb2wnO1xuaW1wb3J0IFNvcnRDb250cm9sIGZyb20gJy4vU29ydENvbnRyb2wnO1xuaW1wb3J0IEZpbHRlckNvbnRyb2wgZnJvbSAnLi9GaWx0ZXJDb250cm9sJztcbmltcG9ydCB7IGxlbmd0aHMgfSBmcm9tICduZXRsaWZ5LWNtcy11aS1kZWZhdWx0JztcblxuY29uc3QgQ29sbGVjdGlvbkNvbnRyb2xzQ29udGFpbmVyID0gc3R5bGVkLmRpdmBcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZmxleC1kaXJlY3Rpb246IHJvdy1yZXZlcnNlO1xuICBtYXJnaW4tdG9wOiAyMnB4O1xuICB3aWR0aDogJHtsZW5ndGhzLnRvcENhcmRXaWR0aH07XG4gIG1heC13aWR0aDogMTAwJTtcblxuICAmID4gZGl2IHtcbiAgICBtYXJnaW4tbGVmdDogNnB4O1xuICB9XG5gO1xuXG5jb25zdCBDb2xsZWN0aW9uQ29udHJvbHMgPSAoe1xuICB2aWV3U3R5bGUsXG4gIG9uQ2hhbmdlVmlld1N0eWxlLFxuICBzb3J0YWJsZUZpZWxkcyxcbiAgb25Tb3J0Q2xpY2ssXG4gIHNvcnQsXG4gIHZpZXdGaWx0ZXJzLFxuICBvbkZpbHRlckNsaWNrLFxuICB0LFxuICBmaWx0ZXIsXG59KSA9PiB7XG4gIHJldHVybiAoXG4gICAgPENvbGxlY3Rpb25Db250cm9sc0NvbnRhaW5lcj5cbiAgICAgIDxWaWV3U3R5bGVDb250cm9sIHZpZXdTdHlsZT17dmlld1N0eWxlfSBvbkNoYW5nZVZpZXdTdHlsZT17b25DaGFuZ2VWaWV3U3R5bGV9IC8+XG4gICAgICB7dmlld0ZpbHRlcnMubGVuZ3RoID4gMCAmJiAoXG4gICAgICAgIDxGaWx0ZXJDb250cm9sXG4gICAgICAgICAgdmlld0ZpbHRlcnM9e3ZpZXdGaWx0ZXJzfVxuICAgICAgICAgIG9uRmlsdGVyQ2xpY2s9e29uRmlsdGVyQ2xpY2t9XG4gICAgICAgICAgdD17dH1cbiAgICAgICAgICBmaWx0ZXI9e2ZpbHRlcn1cbiAgICAgICAgLz5cbiAgICAgICl9XG4gICAgICB7c29ydGFibGVGaWVsZHMubGVuZ3RoID4gMCAmJiAoXG4gICAgICAgIDxTb3J0Q29udHJvbCBmaWVsZHM9e3NvcnRhYmxlRmllbGRzfSBzb3J0PXtzb3J0fSBvblNvcnRDbGljaz17b25Tb3J0Q2xpY2t9IC8+XG4gICAgICApfVxuICAgIDwvQ29sbGVjdGlvbkNvbnRyb2xzQ29udGFpbmVyPlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQ29sbGVjdGlvbkNvbnRyb2xzO1xuIl19 */"));

const CollectionControls = ({
  viewStyle,
  onChangeViewStyle,
  sortableFields,
  onSortClick,
  sort,
  viewFilters,
  onFilterClick,
  t,
  filter
}) => {
  return (0, _core.jsx)(CollectionControlsContainer, null, (0, _core.jsx)(_ViewStyleControl.default, {
    viewStyle: viewStyle,
    onChangeViewStyle: onChangeViewStyle
  }), viewFilters.length > 0 && (0, _core.jsx)(_FilterControl.default, {
    viewFilters: viewFilters,
    onFilterClick: onFilterClick,
    t: t,
    filter: filter
  }), sortableFields.length > 0 && (0, _core.jsx)(_SortControl.default, {
    fields: sortableFields,
    sort: sort,
    onSortClick: onSortClick
  }));
};

var _default = CollectionControls;
exports.default = _default;