"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

var _core = require("@emotion/core");

var _reactPolyglot = require("react-polyglot");

var _netlifyCmsUiDefault = require("netlify-cms-ui-default");

var _redux = require("../../types/redux");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

const SortButton = ( /*#__PURE__*/0, _styledBase.default)(_netlifyCmsUiDefault.StyledDropdownButton, {
  target: "e12k9syt0",
  label: "SortButton"
})(_netlifyCmsUiDefault.buttons.button, ";", _netlifyCmsUiDefault.buttons.medium, ";", _netlifyCmsUiDefault.buttons.grayText, ";font-size:14px;&:after{top:11px;}" + (process.env.NODE_ENV === "production" ? "" : "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0NvbGxlY3Rpb24vU29ydENvbnRyb2wuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBYStDIiwiZmlsZSI6Ii4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0NvbGxlY3Rpb24vU29ydENvbnRyb2wuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IHsgY3NzIH0gZnJvbSAnQGVtb3Rpb24vY29yZSc7XG5pbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCc7XG5pbXBvcnQgeyB0cmFuc2xhdGUgfSBmcm9tICdyZWFjdC1wb2x5Z2xvdCc7XG5pbXBvcnQge1xuICBidXR0b25zLFxuICBEcm9wZG93bixcbiAgRHJvcGRvd25JdGVtLFxuICBTdHlsZWREcm9wZG93bkJ1dHRvbixcbiAgY29sb3JzLFxufSBmcm9tICduZXRsaWZ5LWNtcy11aS1kZWZhdWx0JztcbmltcG9ydCB7IFNvcnREaXJlY3Rpb24gfSBmcm9tICcuLi8uLi90eXBlcy9yZWR1eCc7XG5cbmNvbnN0IFNvcnRCdXR0b24gPSBzdHlsZWQoU3R5bGVkRHJvcGRvd25CdXR0b24pYFxuICAke2J1dHRvbnMuYnV0dG9ufTtcbiAgJHtidXR0b25zLm1lZGl1bX07XG4gICR7YnV0dG9ucy5ncmF5VGV4dH07XG4gIGZvbnQtc2l6ZTogMTRweDtcblxuICAmOmFmdGVyIHtcbiAgICB0b3A6IDExcHg7XG4gIH1cbmA7XG5cbmZ1bmN0aW9uIG5leHRTb3J0RGlyZWN0aW9uKGRpcmVjdGlvbikge1xuICBzd2l0Y2ggKGRpcmVjdGlvbikge1xuICAgIGNhc2UgU29ydERpcmVjdGlvbi5Bc2NlbmRpbmc6XG4gICAgICByZXR1cm4gU29ydERpcmVjdGlvbi5EZXNjZW5kaW5nO1xuICAgIGNhc2UgU29ydERpcmVjdGlvbi5EZXNjZW5kaW5nOlxuICAgICAgcmV0dXJuIFNvcnREaXJlY3Rpb24uTm9uZTtcbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIFNvcnREaXJlY3Rpb24uQXNjZW5kaW5nO1xuICB9XG59XG5cbmZ1bmN0aW9uIHNvcnRJY29uUHJvcHMoc29ydERpcikge1xuICByZXR1cm4ge1xuICAgIGljb246ICdjaGV2cm9uJyxcbiAgICBpY29uRGlyZWN0aW9uOiBzb3J0SWNvbkRpcmVjdGlvbnNbc29ydERpcl0sXG4gICAgaWNvblNtYWxsOiB0cnVlLFxuICB9O1xufVxuXG5jb25zdCBzb3J0SWNvbkRpcmVjdGlvbnMgPSB7XG4gIFtTb3J0RGlyZWN0aW9uLkFzY2VuZGluZ106ICd1cCcsXG4gIFtTb3J0RGlyZWN0aW9uLkRlc2NlbmRpbmddOiAnZG93bicsXG59O1xuXG5jb25zdCBTb3J0Q29udHJvbCA9ICh7IHQsIGZpZWxkcywgb25Tb3J0Q2xpY2ssIHNvcnQgfSkgPT4ge1xuICBjb25zdCBoYXNBY3RpdmVTb3J0ID0gc29ydFxuICAgID8udmFsdWVTZXEoKVxuICAgIC50b0pTKClcbiAgICAuc29tZShzID0+IHMuZGlyZWN0aW9uICE9PSBTb3J0RGlyZWN0aW9uLk5vbmUpO1xuXG4gIHJldHVybiAoXG4gICAgPERyb3Bkb3duXG4gICAgICByZW5kZXJCdXR0b249eygpID0+IChcbiAgICAgICAgPFNvcnRCdXR0b25cbiAgICAgICAgICBjc3M9e2Nzc2BcbiAgICAgICAgICAgIGNvbG9yOiAke2hhc0FjdGl2ZVNvcnQgPyBjb2xvcnMuYWN0aXZlIDogdW5kZWZpbmVkfTtcbiAgICAgICAgICBgfVxuICAgICAgICA+XG4gICAgICAgICAge3QoJ2NvbGxlY3Rpb24uY29sbGVjdGlvblRvcC5zb3J0QnknKX1cbiAgICAgICAgPC9Tb3J0QnV0dG9uPlxuICAgICAgKX1cbiAgICAgIGNsb3NlT25TZWxlY3Rpb249e2ZhbHNlfVxuICAgICAgZHJvcGRvd25Ub3BPdmVybGFwPVwiMzBweFwiXG4gICAgICBkcm9wZG93bldpZHRoPVwiMTYwcHhcIlxuICAgICAgZHJvcGRvd25Qb3NpdGlvbj1cImxlZnRcIlxuICAgID5cbiAgICAgIHtmaWVsZHMubWFwKGZpZWxkID0+IHtcbiAgICAgICAgY29uc3Qgc29ydERpciA9IHNvcnQ/LmdldEluKFtmaWVsZC5rZXksICdkaXJlY3Rpb24nXSk7XG4gICAgICAgIGNvbnN0IGlzQWN0aXZlID0gc29ydERpciAmJiBzb3J0RGlyICE9PSBTb3J0RGlyZWN0aW9uLk5vbmU7XG4gICAgICAgIGNvbnN0IG5leHRTb3J0RGlyID0gbmV4dFNvcnREaXJlY3Rpb24oc29ydERpcik7XG4gICAgICAgIHJldHVybiAoXG4gICAgICAgICAgPERyb3Bkb3duSXRlbVxuICAgICAgICAgICAga2V5PXtmaWVsZC5rZXl9XG4gICAgICAgICAgICBsYWJlbD17ZmllbGQubGFiZWx9XG4gICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiBvblNvcnRDbGljayhmaWVsZC5rZXksIG5leHRTb3J0RGlyKX1cbiAgICAgICAgICAgIGlzQWN0aXZlPXtpc0FjdGl2ZX1cbiAgICAgICAgICAgIHsuLi4oaXNBY3RpdmUgJiYgc29ydEljb25Qcm9wcyhzb3J0RGlyKSl9XG4gICAgICAgICAgLz5cbiAgICAgICAgKTtcbiAgICAgIH0pfVxuICAgIDwvRHJvcGRvd24+XG4gICk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCB0cmFuc2xhdGUoKShTb3J0Q29udHJvbCk7XG4iXX0= */"));

function nextSortDirection(direction) {
  switch (direction) {
    case _redux.SortDirection.Ascending:
      return _redux.SortDirection.Descending;

    case _redux.SortDirection.Descending:
      return _redux.SortDirection.None;

    default:
      return _redux.SortDirection.Ascending;
  }
}

function sortIconProps(sortDir) {
  return {
    icon: 'chevron',
    iconDirection: sortIconDirections[sortDir],
    iconSmall: true
  };
}

const sortIconDirections = {
  [_redux.SortDirection.Ascending]: 'up',
  [_redux.SortDirection.Descending]: 'down'
};

const SortControl = ({
  t,
  fields,
  onSortClick,
  sort
}) => {
  const hasActiveSort = sort === null || sort === void 0 ? void 0 : sort.valueSeq().toJS().some(s => s.direction !== _redux.SortDirection.None);
  return (0, _core.jsx)(_netlifyCmsUiDefault.Dropdown, {
    renderButton: () => (0, _core.jsx)(SortButton, {
      css: /*#__PURE__*/(0, _core.css)("color:", hasActiveSort ? _netlifyCmsUiDefault.colors.active : undefined, ";;label:SortControl;" + (process.env.NODE_ENV === "production" ? "" : "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9jb21wb25lbnRzL0NvbGxlY3Rpb24vU29ydENvbnRyb2wuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBMERrQiIsImZpbGUiOiIuLi8uLi8uLi8uLi9zcmMvY29tcG9uZW50cy9Db2xsZWN0aW9uL1NvcnRDb250cm9sLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCB7IGNzcyB9IGZyb20gJ0BlbW90aW9uL2NvcmUnO1xuaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnO1xuaW1wb3J0IHsgdHJhbnNsYXRlIH0gZnJvbSAncmVhY3QtcG9seWdsb3QnO1xuaW1wb3J0IHtcbiAgYnV0dG9ucyxcbiAgRHJvcGRvd24sXG4gIERyb3Bkb3duSXRlbSxcbiAgU3R5bGVkRHJvcGRvd25CdXR0b24sXG4gIGNvbG9ycyxcbn0gZnJvbSAnbmV0bGlmeS1jbXMtdWktZGVmYXVsdCc7XG5pbXBvcnQgeyBTb3J0RGlyZWN0aW9uIH0gZnJvbSAnLi4vLi4vdHlwZXMvcmVkdXgnO1xuXG5jb25zdCBTb3J0QnV0dG9uID0gc3R5bGVkKFN0eWxlZERyb3Bkb3duQnV0dG9uKWBcbiAgJHtidXR0b25zLmJ1dHRvbn07XG4gICR7YnV0dG9ucy5tZWRpdW19O1xuICAke2J1dHRvbnMuZ3JheVRleHR9O1xuICBmb250LXNpemU6IDE0cHg7XG5cbiAgJjphZnRlciB7XG4gICAgdG9wOiAxMXB4O1xuICB9XG5gO1xuXG5mdW5jdGlvbiBuZXh0U29ydERpcmVjdGlvbihkaXJlY3Rpb24pIHtcbiAgc3dpdGNoIChkaXJlY3Rpb24pIHtcbiAgICBjYXNlIFNvcnREaXJlY3Rpb24uQXNjZW5kaW5nOlxuICAgICAgcmV0dXJuIFNvcnREaXJlY3Rpb24uRGVzY2VuZGluZztcbiAgICBjYXNlIFNvcnREaXJlY3Rpb24uRGVzY2VuZGluZzpcbiAgICAgIHJldHVybiBTb3J0RGlyZWN0aW9uLk5vbmU7XG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBTb3J0RGlyZWN0aW9uLkFzY2VuZGluZztcbiAgfVxufVxuXG5mdW5jdGlvbiBzb3J0SWNvblByb3BzKHNvcnREaXIpIHtcbiAgcmV0dXJuIHtcbiAgICBpY29uOiAnY2hldnJvbicsXG4gICAgaWNvbkRpcmVjdGlvbjogc29ydEljb25EaXJlY3Rpb25zW3NvcnREaXJdLFxuICAgIGljb25TbWFsbDogdHJ1ZSxcbiAgfTtcbn1cblxuY29uc3Qgc29ydEljb25EaXJlY3Rpb25zID0ge1xuICBbU29ydERpcmVjdGlvbi5Bc2NlbmRpbmddOiAndXAnLFxuICBbU29ydERpcmVjdGlvbi5EZXNjZW5kaW5nXTogJ2Rvd24nLFxufTtcblxuY29uc3QgU29ydENvbnRyb2wgPSAoeyB0LCBmaWVsZHMsIG9uU29ydENsaWNrLCBzb3J0IH0pID0+IHtcbiAgY29uc3QgaGFzQWN0aXZlU29ydCA9IHNvcnRcbiAgICA/LnZhbHVlU2VxKClcbiAgICAudG9KUygpXG4gICAgLnNvbWUocyA9PiBzLmRpcmVjdGlvbiAhPT0gU29ydERpcmVjdGlvbi5Ob25lKTtcblxuICByZXR1cm4gKFxuICAgIDxEcm9wZG93blxuICAgICAgcmVuZGVyQnV0dG9uPXsoKSA9PiAoXG4gICAgICAgIDxTb3J0QnV0dG9uXG4gICAgICAgICAgY3NzPXtjc3NgXG4gICAgICAgICAgICBjb2xvcjogJHtoYXNBY3RpdmVTb3J0ID8gY29sb3JzLmFjdGl2ZSA6IHVuZGVmaW5lZH07XG4gICAgICAgICAgYH1cbiAgICAgICAgPlxuICAgICAgICAgIHt0KCdjb2xsZWN0aW9uLmNvbGxlY3Rpb25Ub3Auc29ydEJ5Jyl9XG4gICAgICAgIDwvU29ydEJ1dHRvbj5cbiAgICAgICl9XG4gICAgICBjbG9zZU9uU2VsZWN0aW9uPXtmYWxzZX1cbiAgICAgIGRyb3Bkb3duVG9wT3ZlcmxhcD1cIjMwcHhcIlxuICAgICAgZHJvcGRvd25XaWR0aD1cIjE2MHB4XCJcbiAgICAgIGRyb3Bkb3duUG9zaXRpb249XCJsZWZ0XCJcbiAgICA+XG4gICAgICB7ZmllbGRzLm1hcChmaWVsZCA9PiB7XG4gICAgICAgIGNvbnN0IHNvcnREaXIgPSBzb3J0Py5nZXRJbihbZmllbGQua2V5LCAnZGlyZWN0aW9uJ10pO1xuICAgICAgICBjb25zdCBpc0FjdGl2ZSA9IHNvcnREaXIgJiYgc29ydERpciAhPT0gU29ydERpcmVjdGlvbi5Ob25lO1xuICAgICAgICBjb25zdCBuZXh0U29ydERpciA9IG5leHRTb3J0RGlyZWN0aW9uKHNvcnREaXIpO1xuICAgICAgICByZXR1cm4gKFxuICAgICAgICAgIDxEcm9wZG93bkl0ZW1cbiAgICAgICAgICAgIGtleT17ZmllbGQua2V5fVxuICAgICAgICAgICAgbGFiZWw9e2ZpZWxkLmxhYmVsfVxuICAgICAgICAgICAgb25DbGljaz17KCkgPT4gb25Tb3J0Q2xpY2soZmllbGQua2V5LCBuZXh0U29ydERpcil9XG4gICAgICAgICAgICBpc0FjdGl2ZT17aXNBY3RpdmV9XG4gICAgICAgICAgICB7Li4uKGlzQWN0aXZlICYmIHNvcnRJY29uUHJvcHMoc29ydERpcikpfVxuICAgICAgICAgIC8+XG4gICAgICAgICk7XG4gICAgICB9KX1cbiAgICA8L0Ryb3Bkb3duPlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgdHJhbnNsYXRlKCkoU29ydENvbnRyb2wpO1xuIl19 */"))
    }, t('collection.collectionTop.sortBy')),
    closeOnSelection: false,
    dropdownTopOverlap: "30px",
    dropdownWidth: "160px",
    dropdownPosition: "left"
  }, fields.map(field => {
    const sortDir = sort === null || sort === void 0 ? void 0 : sort.getIn([field.key, 'direction']);
    const isActive = sortDir && sortDir !== _redux.SortDirection.None;
    const nextSortDir = nextSortDirection(sortDir);
    return (0, _core.jsx)(_netlifyCmsUiDefault.DropdownItem, _extends({
      key: field.key,
      label: field.label,
      onClick: () => onSortClick(field.key, nextSortDir),
      isActive: isActive
    }, isActive && sortIconProps(sortDir)));
  }));
};

var _default = (0, _reactPolyglot.translate)()(SortControl);

exports.default = _default;