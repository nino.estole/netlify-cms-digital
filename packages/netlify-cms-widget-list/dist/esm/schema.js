"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  properties: {
    allow_add: {
      type: 'boolean'
    },
    collapsed: {
      type: 'boolean'
    },
    summary: {
      type: 'string'
    },
    minimize_collapsed: {
      type: 'boolean'
    },
    label_singular: {
      type: 'string'
    }
  }
};
exports.default = _default;